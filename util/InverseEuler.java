public class InverseEuler {
    public Matrix solveInverseEuler(Matrix x, Matrix A, Matrix B, Matrix r, double T) {

        Matrix U = new Matrix(A.rows);
        Matrix Acopy = new Matrix(A);
        Matrix Bcopy = new Matrix(B);
        //implement inverse Euler formula
        //P*xk
        Acopy.scalarMultiply(T);
        U.subtractMatrix(Acopy);
        Matrix P = new Matrix(U.rows);
        U.inverse(P);
        Matrix inverseCopy = new Matrix(U);
        U.matrixMultiply(x);

        //Q*r(t)

        inverseCopy.scalarMultiply(T);
        inverseCopy.matrixMultiply(Bcopy);
        inverseCopy.matrixMultiply(r);
        //add
        U.addMatrix(inverseCopy);

        //U.printMatrix();
        return U;
    }

    public void iterateInverseEuler(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {
        Matrix invEuSolved0 = solveInverseEuler(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double analiticX1 = x.data[0][0] * Math.cos(i + T) + x.data[1][0] * Math.sin(i + T);
            double analiticX2 = x.data[1][0] * Math.cos(i + T) - x.data[0][0] * Math.sin(i + T);
            diff += Math.abs(invEuSolved0.data[0][0] - analiticX1) + Math.abs(invEuSolved0.data[1][0] - analiticX2);

            Matrix invEuSolvednext = solveInverseEuler(invEuSolved0, A, B, r, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + invEuSolved0.getElement(0, 0) + "\t\t" + invEuSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            invEuSolved0 = invEuSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }

    public void iterateInverseEuler2(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {
        Matrix invEuSolved0 = solveInverseEuler(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double[][] new_r = {{i}, {i}};
            Matrix new_r_mat = new Matrix(new_r);
            Matrix invEuSolvednext = solveInverseEuler(invEuSolved0, A, B, new_r_mat, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + invEuSolved0.getElement(0, 0) + "\t\t" + invEuSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            invEuSolved0 = invEuSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }
}
