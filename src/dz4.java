import genetic.FunctionAbstractGA;
import genetic.Genetic;
import genetic.GeneticBinary;

public class dz4 {

    public static void main(String[] args) {


        FunctionAbstractGA f1 = new FunctionAbstractGA() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return 100 * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]) + (1 - x[0]) * (1 - x[0]);
            }
        };

        FunctionAbstractGA f3 = new FunctionAbstractGA() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return Math.pow(x[0] - 1, 2) + Math.pow(x[1] - 2, 2) + Math.pow(x[2] - 3, 2) + Math.pow(x[3] - 4, 2) + Math.pow(x[4] - 5, 2);
            }
        };
        FunctionAbstractGA f6 = new FunctionAbstractGA() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                double sum = 0;
                for (int i = 0; i < (x.length - 2); i++) {
                    sum = sum + (x[i] * x[i]);
                }
                double a = (Math.sin(Math.sqrt(sum)) * Math.sin(Math.sqrt(sum))) - 0.5;
                double b = 1 + (0.001 * sum);
                return 0.5 + (a / (b * b));
            }
        };
        FunctionAbstractGA f6_binary = new FunctionAbstractGA() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                double sum = 0;
                for (int i = 0; i < (x.length); i++) {
                    sum = sum + (x[i] * x[i]);
                }
                double a = (Math.sin(Math.sqrt(sum)) * Math.sin(Math.sqrt(sum))) - 0.5;
                double b = 1 + (0.001 * sum);
                return 0.5 + (a / (b * b));
            }
        };

        FunctionAbstractGA f7 = new FunctionAbstractGA() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                double sum = 0;

                for (int i = 0; i < (x.length - 2); i++) {
                    sum += (x[i] * x[i]);
                }
                double a = (Math.pow(sum, 0.25));
                double b = 50 * Math.pow(sum, 0.1);
                double c = 1 + Math.sin(b) * Math.sin(b);
                return a * c;
            }
        };
        FunctionAbstractGA f7_binary = new FunctionAbstractGA() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                double sum = 0;

                for (int i = 0; i < (x.length); i++) {
                    sum += (x[i] * x[i]);
                }
                double a = (Math.pow(sum, 0.25));
                double b = 50 * Math.pow(sum, 0.1);
                double c = 1 + Math.sin(b) * Math.sin(b);
                return a * c;
            }
        };


        int FUNCTION = 7;
        int TASK = 4;
        boolean useBinary = true;

        switch (TASK) {
            ///zadatak
            case 1:
                switch (FUNCTION) {
                    case 1:
                        double eps = 1E-6;
                        double[] min = {-50, -50};
                        double[] max = {150, 150};
                        double mutationChance = 0.3;
                        int dimensions = 2;
                        int precision = 5;
                        int popSize = 20;
                        int maxIter = 100000;
                        int printfreq = 10000;

                        if (!useBinary) {
                            Genetic ga1 = new Genetic(f1, popSize, maxIter, dimensions, min, max, mutationChance, eps, printfreq);
                            ga1.solveGenetic();
                        } else {
                            GeneticBinary ga1 = new GeneticBinary(f1, popSize, maxIter, dimensions, precision, min, max, mutationChance, eps, printfreq);
                            ga1.solveGenetic();
                        }

                        break;
                    case 3:
                        double eps3 = 1E-4;
                        double[] min3 = {-50, -50, -50, -50, -50};
                        double[] max3 = {150, 150, 150, 150, 150};
                        double mutationChance3 = 0.3;
                        int dimensions3 = 2;
                        int precision3 = 5;
                        int popSize3 = 20;
                        int maxIter3 = 100000;
                        int printfreq3 = 10000;

                        if (!useBinary) {
                            Genetic ga3 = new Genetic(f3, popSize3, maxIter3, dimensions3, min3, max3, mutationChance3, eps3, printfreq3);
                            ga3.solveGenetic();
                        } else {
                            GeneticBinary ga3 = new GeneticBinary(f3, popSize3, maxIter3, dimensions3, precision3, min3, max3, mutationChance3, eps3, printfreq3);
                            ga3.solveGenetic();
                        }

                        break;
                    case 6:
                        double eps6 = 1E-4;
                        double[] min6 = {-50, -50};
                        double[] max6 = {150, 150};
                        double mutationChance6 = 0.3;
                        int dimensions6 = 2;
                        int precision6 = 5;
                        int popSize6 = 20;
                        int maxIter6 = 10000;
                        int printfreq6 = 1000;

                        if (!useBinary) {
                            Genetic ga6 = new Genetic(f6, popSize6, maxIter6, dimensions6, min6, max6, mutationChance6, eps6, printfreq6);
                            ga6.solveGenetic();
                        } else {
                            GeneticBinary ga6 = new GeneticBinary(f6_binary, popSize6, maxIter6, dimensions6, precision6, min6, max6, mutationChance6, eps6, printfreq6);
                            ga6.solveGenetic();
                        }
                        break;

                    case 7:
                        double eps7 = 1E-6;
                        double[] min7 = {-50, -50};
                        double[] max7 = {150, 150};
                        double mutationChance7 = 0.3;
                        int dimensions7 = 2;
                        int precision7 = 5;
                        int popSize7 = 20;
                        int maxIter7 = 100000;
                        int printfreq7 = 10000;

                        if (!useBinary) {
                            Genetic ga7 = new Genetic(f7, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                            ga7.solveGenetic();
                        } else {
                            GeneticBinary ga7 = new GeneticBinary(f7_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                            ga7.solveGenetic();
                        }
                        break;

                }
                break;
            ///zadatak
            case 2:
                double eps7 = 1E-6;
                double[] min7 = {-50, -50, -50, -50, -50, 50, -50, -50, -50, -50};
                ;
                double[] max7 = {150, 150, 150, 150, 150, 150, 150, 150, 150, 150};
                double mutationChance7 = 0.3;
                int dimensions7 = 1;
                int precision7 = 5;
                int popSize7 = 20;
                int maxIter7 = 2000;
                int printfreq7 = 10000000;
                System.out.println("Dimensions: " + dimensions7 + "\tfunction7");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f7, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f7_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                f7.resetCounter();
                f7_binary.resetCounter();
                dimensions7 = 3;
                System.out.println("Dimensions: " + dimensions7 + "\tfunction7");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f7, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f7_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                f7.resetCounter();
                f7_binary.resetCounter();
                dimensions7 = 6;
                System.out.println("Dimensions: " + dimensions7 + "\tfunction7");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f7, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f7_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                f7.resetCounter();
                f7_binary.resetCounter();
                dimensions7 = 10;
                System.out.println("Dimensions: " + dimensions7 + "\tfunction7");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f7, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f7_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                f7.resetCounter();
                f7_binary.resetCounter();
                /////////////////////FUNCTION 6
                dimensions7 = 1;
                System.out.println("\n\n\nDimensions: " + dimensions7 + "\tfunction6");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f6, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f6_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                f6.resetCounter();
                f6_binary.resetCounter();

                dimensions7 = 3;
                System.out.println("Dimensions: " + dimensions7 + "\tfunction6");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f6, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f6_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                f6.resetCounter();
                f6_binary.resetCounter();
                dimensions7 = 6;
                System.out.println("Dimensions: " + dimensions7 + "\tfunction6");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f6, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f6_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                f6.resetCounter();
                f6_binary.resetCounter();
                dimensions7 = 10;
                System.out.println("Dimensions: " + dimensions7 + "\tfunction6");
                if (!useBinary) {
                    Genetic ga7 = new Genetic(f6, popSize7, maxIter7, dimensions7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                } else {
                    GeneticBinary ga7 = new GeneticBinary(f6_binary, popSize7, maxIter7, dimensions7, precision7, min7, max7, mutationChance7, eps7, printfreq7);
                    ga7.solveGenetic();
                }
                break;

            ///zadatak
            case 4:
                double eps6 = 1E-6;
                double[] min6 = {-50, -50};
                double[] max6 = {150, 150};
                double mutationChance6 = 0.1;
                int dimensions6 = 2;
                int precision6 = 5;
                int popSize6 = 100;
                int maxIter6 = 100000;
                int printfreq6 = 100000;

                if (!useBinary) {
                    Genetic ga6 = new Genetic(f6, popSize6, maxIter6, dimensions6, min6, max6, mutationChance6, eps6, printfreq6);
                    ga6.solveGenetic();
                } else {
                    GeneticBinary ga6 = new GeneticBinary(f6_binary, popSize6, maxIter6, dimensions6, precision6, min6, max6, mutationChance6, eps6, printfreq6);
                    ga6.solveGenetic();
                }
                break;

        }

    }
}
