package genetic;

public class Cromosome {
    public String[] binary = new String[GeneticBinary.dimensions];
    public double[] originalDecimal = new double[GeneticBinary.dimensions];
    public int[] decimal = new int[GeneticBinary.dimensions];
    public double f_value = 0;
    public double fitness = 0;

    public int[] getDecimal() {
        return decimal;
    }

    public void setDecimal(int[] decimal) {
        this.decimal = decimal;
    }

    public double getF_value() {
        return f_value;
    }

    public void setF_value(double f_value) {
        this.f_value = f_value;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public double[] getOriginalDecimal() {
        return originalDecimal;
    }

    public void setOriginalDecimal(double[] originalDecimal) {
        this.originalDecimal = originalDecimal;
    }

    public String[] getBinary() {
        return binary;
    }

    public void setBinary(String[] binary) {
        this.binary = binary;
    }


}
