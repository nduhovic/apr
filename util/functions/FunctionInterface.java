package functions;

public interface FunctionInterface {
    double solvef(double x[]);

    double[] solvef_1d(double[] x);

    double[][] solve_hessian(double[] x);

}
