package functions;

public class FunctionTwo implements FunctionInterface {
    int i;
    double[] x0;
    FunctionInterface f;
    public int counter = 0;
    public int gradCounter = 0;
    public int hessCounter = 0;

    public FunctionTwo(double[] x0) {
        this.i = i;
        this.x0 = x0;
        this.f = f;

    }

    @Override
    public double solvef(double[] x) {
        this.incCounter();
        return (x[0] - 4) * (x[0] - 4) + 4 * (x[1] - 2) * (x[1] - 2);

    }

    @Override
    public double[] solvef_1d(double[] x) {
        this.incGradCounter();
        double der_x1 = 2 * (x[0] - 4);
        double der_x2 = 8 * (x[1] - 2);
        return new double[]{der_x1, der_x2};
    }

    void incCounter() {
        this.counter++;
    }

    public void resetCounter() {
        this.counter = 0;
    }

    void incGradCounter() {
        this.gradCounter++;
    }

    public void resetGradCounter() {
        this.gradCounter = 0;
    }

    void incHessCounter() {
        this.hessCounter++;
    }

    public void resetHessCounter() {
        this.hessCounter = 0;
    }

    public void resetAllCounters() {
        this.counter = 0;
        this.gradCounter = 0;
        this.hessCounter = 0;
    }

    public double[][] solve_hessian(double[] x) {
        this.incHessCounter();
        double der_x1_x1 = 2;
        double der_x1_x2 = 0;
        double der_x2_x1 = 0;
        double der_x2_x2 = 8;
        return new double[][]{{der_x1_x1, der_x1_x2,}, {der_x2_x1, der_x2_x2}};
    }
}
