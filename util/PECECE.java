public class PECECE {


    public Matrix solvePECECE(Matrix x, Matrix A, Matrix B, Matrix r, double T) {
        Euler euler = new Euler();
        Trapese trapese = new Trapese();
        Matrix x_i = new Matrix(x);
        x_i = euler.solveEuler(x, A, B, r, T);
        //correction, inverse Euler 1st time
        Matrix Xcopy = new Matrix(x);
        Matrix Acopy = new Matrix(A);
        Matrix Bcopy = new Matrix(B);
        Acopy.matrixMultiply(x_i);
        Bcopy.matrixMultiply(r);
        Acopy.addMatrix(Bcopy);
        Acopy.scalarMultiply(T);
        Xcopy.addMatrix(Acopy);
        //correction, inverse Euler 2nd time
        Matrix Xcopy2 = new Matrix(x);
        Matrix Acopy2 = new Matrix(A);
        Matrix Bcopy2 = new Matrix(B);
        Acopy2.matrixMultiply(Xcopy);
        Bcopy2.matrixMultiply(r);
        Acopy2.addMatrix(Bcopy);
        Acopy2.scalarMultiply(T);
        Xcopy2.addMatrix(Acopy);

        return Xcopy2;
    }

    public void iteratePECECE(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {

        Matrix PECECESolved0 = solvePECECE(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double analiticX1 = x.data[0][0] * Math.cos(i + T) + x.data[1][0] * Math.sin(i + T);
            double analiticX2 = x.data[1][0] * Math.cos(i + T) - x.data[0][0] * Math.sin(i + T);
            diff += Math.abs(PECECESolved0.data[0][0] - analiticX1) + Math.abs(PECECESolved0.data[1][0] - analiticX2);


            Matrix trapSolvednext = solvePECECE(PECECESolved0, A, B, r, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + PECECESolved0.getElement(0, 0) + "\t\t" + PECECESolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            PECECESolved0 = trapSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }

    public void iteratePECECE2(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {

        Matrix PECECESolved0 = solvePECECE(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double[][] new_r = {{i}, {i}};
            Matrix new_r_mat = new Matrix(new_r);
            Matrix trapSolvednext = solvePECECE(PECECESolved0, A, B, new_r_mat, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + PECECESolved0.getElement(0, 0) + "\t\t" + PECECESolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            PECECESolved0 = trapSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }
}

