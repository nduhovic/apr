public class Trapese {
    public Matrix solveTrapese(Matrix x, Matrix A, Matrix B, Matrix r, double T) {

        Matrix U = new Matrix(A.rows);
        Matrix UCopy = new Matrix(U);

        Matrix Acopy = new Matrix(A);
        Matrix Bcopy = new Matrix(B);
        Matrix Rcopy = new Matrix(r);
        //implement inverse Euler formula
        //R*xk
        double Thalf = T / 2;

        Acopy.scalarMultiply(Thalf);
        U.subtractMatrix(Acopy);
        Matrix P = new Matrix(U.rows);
        Matrix UCopy2 = new Matrix(U);
        U.inverse(P);
        UCopy.addMatrix(Acopy);
        U.matrixMultiply(UCopy);
        U.matrixMultiply(x);

        //S*r(t)
        Matrix P2 = new Matrix(UCopy2.rows);
        UCopy2.inverse(P2);
        UCopy2.scalarMultiply(Thalf);
        UCopy2.matrixMultiply(Bcopy);
        Rcopy.addMatrix(r);
        UCopy2.matrixMultiply(Rcopy);
        //add
        U.addMatrix(UCopy2);

        //U.printMatrix();
        return U;
    }

    public void iterateTrapese(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {

        Matrix trapSolved0 = solveTrapese(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double analiticX1 = x.data[0][0] * Math.cos(i + T) + x.data[1][0] * Math.sin(i + T);
            double analiticX2 = x.data[1][0] * Math.cos(i + T) - x.data[0][0] * Math.sin(i + T);
            diff += Math.abs(trapSolved0.data[0][0] - analiticX1) + Math.abs(trapSolved0.data[1][0] - analiticX2);


            Matrix trapSolvednext = solveTrapese(trapSolved0, A, B, r, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + trapSolved0.getElement(0, 0) + "\t\t" + trapSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            trapSolved0 = trapSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }

    public void iterateTrapese2(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {

        Matrix trapSolved0 = solveTrapese(x, A, B, r, T);
        double i = 0;
        int iter = 0;
        do {
            double[][] new_r = {{i}, {i}};
            Matrix new_r_mat = new Matrix(new_r);

            Matrix trapSolvednext = solveTrapese(trapSolved0, A, B, new_r_mat, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + trapSolved0.getElement(0, 0) + "\t\t" + trapSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            trapSolved0 = trapSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
    }

}
