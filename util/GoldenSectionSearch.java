import functions.FunctionInterface;

public class GoldenSectionSearch {

    private static double k = 0.5 * (Math.sqrt(5) - 1);

    /**
     * Trazenje unimodalnog intervala
     *
     * @param f  funkcija
     * @param h  pocetni korak pretrazivanja
     * @param xo pocetna tocka pretraživanja
     * @return interval [l,r]
     */
    public static double[] unimodalInterval(FunctionInterface f, double h, double xo[]) {

        double[] x0 = {xo[0]};
        double l[] = {x0[0] - h};
        double r[] = {x0[0] + h};
        double m = x0[0];
        double fl, fm, fr;
        int step = 1;
        double[] rez = {l[0], r[0]};

        fm = f.solvef(x0);
        fl = f.solvef(l);
        fr = f.solvef(r);

        if (fm < fr && fm < fl) {
            return rez;
        } else if (fm > fr) {
            do {
                l[0] = m;
                m = r[0];
                fm = fr;
                r[0] = x0[0] + h * (step *= 2);
                fr = f.solvef(r);
            } while (fm > fr);
        } else {
            do {
                r[0] = m;
                m = l[0];
                fm = fl;
                l[0] = x0[0] - h * (step *= 2);
                fl = f.solvef(l);
            } while (fm > fl);
        }

        rez[0] = l[0];
        rez[1] = r[0];
        //System.out.println("unimodalni\n\n___________"+Arrays.toString(rez));
        return rez;
    }

    public static double goldenSection(FunctionInterface f, double[] interval, double e) {

        double a[] = {interval[0]};
        double b[] = {interval[1]};
        double c[] = {b[0] - k * (b[0] - a[0])};
        double d[] = {a[0] + k * (b[0] - a[0])};

        double fc[] = {f.solvef(c)};
        double fd[] = {f.solvef(d)};

        int counter = 0;

        while ((b[0] - a[0]) > e) {

            if (fc[0] < fd[0]) {
                b[0] = d[0];
                d[0] = c[0];
                c[0] = b[0] - k * (b[0] - a[0]);
                fd[0] = fc[0];
                fc[0] = f.solvef(c);
            } else {
                a[0] = c[0];
                c[0] = d[0];
                d[0] = a[0] + k * (b[0] - a[0]);
                fc[0] = fd[0];
                fd[0] = f.solvef(d);
            }
            counter++;
        }
        //System.out.println("rez " + (a[0] + b[0]) / 2);
        //System.out.println("zlatbi\n\n___________"+Arrays.toString(a));
        return (a[0] + b[0]) / 2;
    }

    public static double goldenSectionPoint(FunctionInterface f, double x0[], double e) {
        double interval[] = GoldenSectionSearch.unimodalInterval(f, 1, x0);
        double rez = GoldenSectionSearch.goldenSection(f, interval, e);
        return rez;
    }


}
