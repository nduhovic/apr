package functions;

import java.util.Arrays;

public class Function2 implements FunctionInterface {

    int i;
    double[] x0;
    FunctionInterface f;

    public Function2(int i, double[] x0, FunctionInterface f) {
        this.i = i;
        this.x0 = x0;
        this.f = f;

    }

    @Override
    public double solvef(double[] x) {
        double lambda = x[0];// posto je vektor jednodimenzionalni kod unimodalnog i zlatnog reza
        double[] copy = Arrays.copyOf(x0, x0.length);
        copy[i] += lambda;
        return f.solvef(copy);
    }

    @Override
    public double[] solvef_1d(double[] x) {
        return null;
    }

    @Override
    public double[][] solve_hessian(double[] x) {
        return null;
    }

}