import functions.FunctionInterface;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Box {
    public void solveBox(FunctionInterface f, double[] x0, double alfa, double eps, double[] min, double[] max, FunctionInterface[] implicitConstrains) {
        double[][] boxPoints = new double[2 * x0.length][x0.length];
        int worst = 0;
        int secondWorst = 1;
        int diverCount = 0;
        int iterDiverCount = 0;
        int iter = 0;
        for (int i = 0; i < boxPoints[0].length; i++) {
            for (int j = 0; j < boxPoints.length; j++) {
                boxPoints[j][i] = x0[i];
            }
        }
        //System.out.println(Arrays.deepToString(boxPoints));
        //System.out.println("Centroid" + Arrays.toString(centroid(boxPoints)));
        double[] centroid = centroid(boxPoints);
        double checkDivergenceOld = f.solvef(centroid);
        if (checkExplicit(x0, min, max) && (checkImplicit(x0, implicitConstrains))) {

            for (int t = 0; t < 2 * x0.length; t++) {
                for (int i = 0; i < x0.length; i++) {
                    double random = ThreadLocalRandom.current().nextDouble(0, 1);
                    boxPoints[t][i] = min[i] + random * (max[i] - min[i]);
                }
                while (!checkImplicit(boxPoints[t], implicitConstrains)) {
                    diverCount++;
                    for (int i = 0; i < boxPoints[t].length; i++) {
                        boxPoints[t][i] = ((0.5) * (boxPoints[t][i] + centroid[i]));
                    }
                    if (diverCount > 100) {
                        //System.out.println(Arrays.deepToString(boxPoints));
                        System.out.println("Divergencija!");
                        return;
                    }
                    //System.out.println(Arrays.toString(boxPoints[t]));
                }
                diverCount = 0;
                centroid = centroid(boxPoints);
            }

            //System.out.println(Arrays.deepToString(boxPoints));
        } else {
            System.out.println("Pocetna tocka ne zadovoljava ogranicenja");
            return;
        }
        do {
            iter++;
            //odredi indekse h, h2 : F(X[h]) = max, F(X[h2]) = drugi najlosiji;
            double value0 = f.solvef(boxPoints[0]);
            double value1 = f.solvef(boxPoints[1]);
            secondWorst = 1;
            worst = 0;
            if (value1 >= value0) {
                worst = 1;
                secondWorst = 0;
            }
            if (boxPoints.length > 2) {
                for (int i = 2; i < boxPoints.length; i++) {
                    if (f.solvef(boxPoints[i]) > f.solvef(boxPoints[worst])) {
                        secondWorst = worst;
                        worst = i;
                    } else if (f.solvef(boxPoints[i]) > f.solvef(boxPoints[secondWorst])) {
                        secondWorst = i;
                    }
                }
            }


            centroid = centroidWithoutWorst(boxPoints, worst);

            double[] xr = reflection(boxPoints, centroid, worst, alfa);

            for (int i = 0; i < xr.length; i++) {
                if (xr[i] < min[i]) xr[i] = min[i];
                else if (xr[i] > max[i]) xr[i] = max[i];
            }
            //System.out.println("nakon provjere " + Arrays.toString(xr));

            for (int t = 0; t < boxPoints.length; t++) {
                while (!checkImplicit(xr, implicitConstrains)) {
                    diverCount++;
                    for (int i = 0; i < boxPoints[t].length; i++) {
                        xr[i] = ((0.5) * (xr[i] + centroid[i]));
                    }
                    if (diverCount > 100) {
                        //System.out.println(Arrays.deepToString(boxPoints));
                        System.out.println("Divergencija!");
                        return;
                    }
                    //System.out.println(Arrays.toString(boxPoints[t]));
                }
                diverCount = 0;
            }
            if (f.solvef(xr) > f.solvef(boxPoints[secondWorst])) {
                for (int i = 0; i < xr.length; i++) {
                    xr[i] = ((0.5) * (xr[i] + centroid[i]));
                }
            }
            for (int i = 0; i < xr.length; i++) {
                boxPoints[worst][i] = xr[i];
            }
            double checkDivergenceNew = f.solvef(centroid);
            if (checkDivergenceNew > checkDivergenceOld) {
                iterDiverCount++;
            } else {
                iterDiverCount = 0;
                checkDivergenceOld = checkDivergenceNew;
            }
            if (iterDiverCount > 100) {
                System.out.println("Nema poboljsanja u zadnjih 100 iteracija");
                break;
            }
            //System.out.println(Arrays.toString(centroidWithoutWorst(boxPoints, worst)));
        } while (!stopCondition(boxPoints, worst, eps));
        double[] x_min = centroidWithoutWorst(boxPoints, worst);
        System.out.println("Iteracija:" + iter + "\nx_min: " + Arrays.toString(x_min));
        System.out.println("Vrijednost fje u x_min:" + f.solvef(centroid));
    }

    boolean checkExplicit(double[] x0, double[] min, double[] max) {
        boolean flag = true;
        for (int i = 0; i < x0.length; i++) {
            if (x0[i] > max[i] || x0[i] < min[i]) {
                flag = false;
                //System.out.println("Eksplicitno ograničenje nije zadovoljeno");
                break;
            }
        }
        return flag;
    }

    boolean checkImplicit(double[] x, FunctionInterface[] constrains) {
        boolean flag = true;
        for (int i = 0; i < constrains.length; i++) {
            if (constrains[i].solvef(x) < 0) {
                flag = false;
                //System.out.println("Implicitno ograničenje nije zadovoljeno");
                break;
            }
            //System.out.println("Implicit OK");
        }
        return flag;
    }

    double[] centroid(double[][] boxPoints) {
        double[] xc = new double[2];
        for (int i = 0; i < boxPoints[0].length; i++) {
            for (int j = 0; j < boxPoints.length; j++) {
                xc[i] += boxPoints[j][i];
            }
            xc[i] /= boxPoints.length;
        }
        return xc;
    }

    private double[] reflection(double[][] x, double[] xc, int h, double alfa) {
        double[] xr = new double[xc.length];
        for (int i = 0; i < xc.length; i++) {
            xr[i] = (1 + alfa) * xc[i] - alfa * x[h][i];
        }
        return xr;
    }

    private double[] centroidWithoutWorst(double[][] boxPoints, int h) {
        double[] c = new double[boxPoints[0].length];
        for (int i = 0; i < boxPoints[0].length; i++) {
            c[i] = 0.0;
        }

        for (int i = 0; i < boxPoints[0].length; i++) {
            for (int j = 0; j < boxPoints.length; j++) {
                c[i] += boxPoints[j][i];


            }
        }

        for (int i = 0; i < boxPoints[0].length; i++) {  //remove worst point from sum
            c[i] -= boxPoints[h][i];
        }
        for (int i = 0; i < boxPoints[0].length; i++) {
            c[i] /= boxPoints.length - 1;
        }

        //System.out.println("centorid" + Arrays.toString(c));
        return c;
    }

    private boolean stopCondition(double[][] boxPoints, int h, double eps) {

        boolean flag = true;

        double[] centroid = centroidWithoutWorst(boxPoints, h);
        for (int i = 0; i < boxPoints.length; i++) {
            double sum = 0;
            if (i != h) {
                for (int j = 0; j < boxPoints[0].length; j++) {
                    sum += Math.pow((boxPoints[i][j] - centroid[j]), 2);
                }
                if (Math.sqrt(sum) > eps) {
                    flag = false;

                    return flag;
                }
            }
        }
        return flag;
    }
}