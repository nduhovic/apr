import functions.FunctionInterface;

import java.util.Arrays;

public class HookeJeeves {
    public double e = 10e-6;
    //public double dx = 0.5;

    public double[] solveHookeJeeves(FunctionInterface f, double[] x0, boolean print) {
        int dimension = x0.length;
        double[] xn = new double[dimension];
        double dx = 0.5;

        //xp = x0;
        double[] xb = Arrays.copyOf(x0, x0.length);
        double[] xp = Arrays.copyOf(x0, x0.length);
        int counter = 0;
        do {

            counter++;
            xn = explore(f, xp, dx);
            if (print == true) {
                System.out.println("i: " + counter +
                        " xb: " + Arrays.toString(xb) +
                        " xp: " + Arrays.toString(xp) +
                        " xn:" + Arrays.toString(xn) +
                        "\n\t(xb)" + f.solvef(xb) +
                        "f(xp)" + f.solvef(xp) +
                        "f(xn)" + f.solvef(xn));
            }
            if (f.solvef(xn) < f.solvef(xb)) {
                for (int i = 0; i < x0.length; i++) {
                    xp[i] = 2 * xn[i] - xb[i];
                }
                xb = xn;
            } else {
                dx = dx / 2;
                // System.out.println("dx = " + dx);
                //System.out.println("x_b= " + Arrays.toString(xb));
                xp = xb;
            }
            if (counter > 10000) break;
        } while (dx > e);

        //System.out.println("iterations" + counter);
        if (print == true) System.out.println("x_min = " + Arrays.toString(xb) + "\tf_min = " + f.solvef(xb));
        return xb;
    }

    private double[] explore(FunctionInterface f, double[] xp, double dx) {
        double[] x = new double[xp.length];
        x = Arrays.copyOf(xp, xp.length);
        for (int i = 0; i < xp.length; i++) {
            double P = f.solvef(x);
            x[i] = x[i] + dx;
            double N = f.solvef(x);
            if (N > P) {
                x[i] = x[i] - 2 * dx;
                N = f.solvef(x);
                if (N > P) {
                    x[i] = x[i] + dx;
                }
            }
        }
        return x;
    }
}
