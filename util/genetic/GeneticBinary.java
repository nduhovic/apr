package genetic;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class GeneticBinary {
    static FunctionAbstractGA f;
    static int popSize;
    static int dimensions;
    static double[] min;
    static double[] max;
    static double eps;
    public Cromosome[] population;
    int elitismNo = 1;
    int printFreq;
    double muutationChance;
    int fCounterMax = 1000000000;
    int maxiter;
    int precision;
    int[] n;


    public GeneticBinary(FunctionAbstractGA f, int popSize, int maxiter, int dimensions, int precision, double[] min, double[] max, double mutationChance, double eps, int printFreq) {
        this.f = f;
        this.popSize = popSize;
        this.dimensions = dimensions;
        this.min = min;
        this.max = max;
        this.eps = eps;
        this.muutationChance = mutationChance;
        this.elitismNo = popSize / 10;
        this.printFreq = printFreq;
        this.maxiter = maxiter;
        this.precision = precision;
    }

    public Cromosome[] solveGenetic() {
        this.population = initPopulation();
        //update fitness
        /*for (int i = 0; i < popSize; i++) {
            population[i].setFitness(calcFitness(this.population[i]));
            /*System.out.println("Cromosome decimal:" + Arrays.toString(population[i].originalDecimal) +
                    "\t cromosome dec2: " + Arrays.toString(population[i].decimal) +
                    "\t cromosome binary" + Arrays.toString(population[i].binary) +
                    "\t fitness: " + population[i].getFitness() +
                    "\t f(x):" + population[i].getF_value());
        }*/
        updateFitness();
        double fitnessSum = fitnessSum();


        int generationNo = 0;

        while (f.getCounter() < fCounterMax) {
            Cromosome[] new_population = new Cromosome[popSize];
            //System.out.println("ITER :" + generationNo);
            generationNo++;
            int countChromosomes = 0;
            //elitism
            /*int[] sorted = sort();
            for (int i = 0; i < popSize; ++i) {
                if (sorted[i] < elitismNo) {
                    new_population[countChromosomes] = this.population[sorted[i]];
                    countChromosomes++;
                }
            }
            */
            new_population[0] = this.population[findBest()];
            countChromosomes++;
            while (countChromosomes < popSize) {
                //crossover selection
                int selected1 = rouletteWheelSelection(fitnessSum);
                int selected2 = rouletteWheelSelection(fitnessSum);
                //System.out.println("sel:" + selected1 + "\t" + selected2);
                Cromosome child = new Cromosome();
                while (selected1 == selected2) selected2 = rouletteWheelSelection(fitnessSum);
                //crossover
                double randomCrossoverNo = ThreadLocalRandom.current().nextInt(1, 3);
                if (randomCrossoverNo == 1) {
                    child = crossover1(population[selected1], population[selected2]);
                } else if (randomCrossoverNo == 2) child = crossover2(population[selected1], population[selected2]);

                //mutate

                double mutateRandom = ThreadLocalRandom.current().nextDouble(0, 1);
                if (mutateRandom < muutationChance) {
                    for (int j = 0; j < dimensions; j++) {
                        child = mutate(child);
                    }
                    child.setF_value(f.solvef(child.getOriginalDecimal()));
                    child.setFitness(calcFitness(child));
                }
                new_population[countChromosomes] = child;
                countChromosomes++;

            }

            this.population = new_population;
            for (int i = 0; i < popSize; i++) {

                population[i].setFitness(calcFitness(this.population[i]));
            }
            updateFitness();
            int best;
            best = findBest();
            if (generationNo % printFreq == 0) {

                //int best = findBest();
                //System.out.println("best " + best);

                System.out.println("Generation: " + generationNo + "Cromosome decimal:" + Arrays.toString(population[best].getOriginalDecimal()) +
                        "\t cromosome dec2: " + Arrays.toString(population[best].getDecimal()) +
                        "\t cromosome binary" + Arrays.toString(population[best].getBinary()) +
                        "\t fitness: " + population[best].getFitness() +
                        "\t f(x):" + population[best].getF_value());
            }

            //System.out.println(generationNo);
            if (generationNo > maxiter || Math.abs(population[best].getF_value()) < eps) {
                System.out.println("_______FINISHED________");

                int bestX = findBest();

                System.out.println("Generation: " + generationNo + "\tBest x:" + Arrays.toString(population[bestX].getOriginalDecimal()) +
                        "\t f(x_best):" + population[bestX].getF_value() + "\t F evaluations: " + f.getCounter());

                return this.population;
            }
        }

        return this.population;
    }

    public Cromosome[] initPopulation() {
        Cromosome[] population = new Cromosome[popSize];
        for (int i = 0; i < popSize; i++) {
            Cromosome newCromosome = new Cromosome();
            double[] newDecimal = new double[dimensions];
            for (int j = 0; j < dimensions; j++) {
                newDecimal[j] = ThreadLocalRandom.current().nextDouble(min[j], max[j]);
            }
            newCromosome.setOriginalDecimal(newDecimal);
            newCromosome.setDecimal(convertToDecimal(newCromosome.originalDecimal));
            newCromosome.setBinary(saveBinary(newCromosome));
            newCromosome.setF_value(f.solvef(newCromosome.originalDecimal));

            population[i] = newCromosome;


        }

        return population;
    }

    public String[] saveBinary(Cromosome cromosome) {

        String[] binary = new String[dimensions];
        for (int j = 0; j < dimensions; j++) {
            binary[j] = "";
        }
        for (int j = 0; j < dimensions; j++) {

            binary[j] = Integer.toBinaryString(cromosome.getDecimal()[j]);
            while (binary[j].length() != n[j]) binary[j] = '0' + binary[j];
        }

        return binary;
    }

    public int[] convertToDecimal(double[] x) {
        int[] n = new int[dimensions];
        int[] decimal = new int[dimensions];
        for (int j = 0; j < dimensions; j++) {
            int possibleCombinations = (int) ((max[j] - min[j]) * Math.pow(10, precision));


            while (Math.pow(2, n[j]) < possibleCombinations) n[j]++;


            decimal[j] = (int) (((x[j] - min[j]) / (max[j] - min[j])) * (Math.pow(2, n[j]) - 1));
        }
        this.n = n;
        return decimal;
    }

    public double[] convertToRealDecimal(int[] x) {

        double[] realDecimal = new double[dimensions];
        for (int j = 0; j < dimensions; j++) {
            realDecimal[j] = (double) (((double) x[j] / (Math.pow(2, n[j]) - 1)) * (max[j] - min[j])) + min[j];
        }
        return realDecimal;
    }

    public double calcFitness(Cromosome cromosome) {
        double fitness = 100;
        double fWorst = this.population[findWorst()].getF_value();
        double fBest = this.population[findBest()].getF_value();
        //System.out.println("BEst" + fBest + "worst " + fWorst);
        if (Math.abs(fWorst - fBest) < eps) fitness = 100;
        else
            fitness = 1 + ((100 - 1) * (f.solvef(cromosome.getOriginalDecimal()) - fWorst) / (fBest - fWorst));
        return fitness;
    }

    public int findBest() {
        int best = 0;
        double minValue = population[0].getF_value();
        for (int i = 1; i < popSize; i++) {
            if (Math.abs(population[i].getF_value()) < minValue) {
                minValue = population[i].getF_value();
                best = i;
            }
        }

        return best;
    }

    public int findWorst() {
        int worst = 0;
        double maxValue = population[0].getF_value();
        for (int i = 1; i < popSize; i++) {
            if (Math.abs(this.population[i].getF_value()) > maxValue) {
                maxValue = this.population[i].getF_value();
                worst = i;
            }
        }
        return worst;
    }

    public int[] sort() {
        boolean done = false;
        int[] allF = new int[popSize];
        int[] sorted = new int[popSize];
        Cromosome[] popCopy = population;
        for (int i = 0; i < popSize; i++) {
            sorted[i] = i;
            popCopy[i] = population[i];
        }
        int temp = 0;
        Cromosome tempCr = new Cromosome();
        int n = sorted.length;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (popCopy[j - 1].getF_value() > popCopy[j].getF_value()) {
                    temp = sorted[j - 1];
                    tempCr = popCopy[j - 1];
                    sorted[j - 1] = sorted[j];
                    popCopy[j - 1] = popCopy[j];
                    sorted[j] = temp;
                    popCopy[j] = tempCr;

                }
            }
        }
        return sorted;
    }

    //total fitness of the population
    public double fitnessSum() {
        //System.out.println("called");
        double sum = 0.0;
        for (int i = 0; i < popSize; i++) {
            sum += Math.abs(this.population[i].getFitness());
        }
        //System.out.println(sum);
        return sum;

    }

    public int rouletteWheelSelection(double fitnessSum) {
        int selected = popSize;
        double random = ThreadLocalRandom.current().nextDouble(0, fitnessSum);
        double sum = 0.0;
        for (int i = 0; i < popSize; i++) {
            sum += population[i].getFitness();
            if (random < sum) {
                selected = i;
                if (selected == popSize) return popSize - 1;
                return selected;
            }
        }
        if (selected == popSize) return popSize - 1;
        return selected;
    }

    Cromosome crossover1(Cromosome parent1, Cromosome parent2) {
        Cromosome child = new Cromosome();
        String[] childString = new String[dimensions];
        int[] childDecimal = new int[dimensions];
        double[] childRealDecimal = new double[dimensions];
        for (int i = 0; i < dimensions; i++) {
            childString[i] = "";
            for (int j = 0; j < n[i]; j++) {
                if (j < n[i] / 2) childString[i] += parent1.binary[i].charAt(j);
                else childString[i] += parent2.binary[i].charAt(j);
            }
            childDecimal[i] = Integer.parseInt(childString[i], 2);


        }

        childRealDecimal = convertToRealDecimal(childDecimal);
        for (int i = 0; i < dimensions; i++) {
            if (childRealDecimal[i] > max[i] - 10) {
                childRealDecimal[i] = max[i] - 10;
                childDecimal = convertToDecimal(childRealDecimal);
                childString = saveBinary(child);
            }
        }
        for (int i = 0; i < dimensions; i++) {
            if (childRealDecimal[i] < min[i] + 10) {
                childRealDecimal[i] = min[i] + 10;
                childDecimal = convertToDecimal(childRealDecimal);
                childString = saveBinary(child);
            }
        }
        child.setBinary(childString);
        child.setDecimal(childDecimal);
        child.setOriginalDecimal(childRealDecimal);
        child.setF_value(f.solvef(child.getOriginalDecimal()));
        child.setFitness(calcFitness(child));
        //System.out.println("Str " + childString[0] + " decimal " + childDecimal[0] + "\tp1   " + parent1.binary[0] + "   p2  " + parent2.binary[0] );
        return child;
    }

    Cromosome crossover2(Cromosome parent1, Cromosome parent2) {
        Cromosome child = new Cromosome();
        double[] childRealDecimal = new double[dimensions];
        String[] childString = new String[dimensions];
        int[] childDecimal = new int[dimensions];
        for (int i = 0; i < dimensions; i++) {
            childString[i] = "";
            for (int j = 0; j < n[i]; j++) {
                if (j % 2 == 0) childString[i] += parent1.binary[i].charAt(j);
                else childString[i] += parent2.binary[i].charAt(j);
            }
            childDecimal[i] = Integer.parseInt(childString[i], 2);

        }
        childRealDecimal = convertToRealDecimal(childDecimal);
        for (int i = 0; i < dimensions; i++) {
            if (childRealDecimal[i] > max[i] - 10) {
                childRealDecimal[i] = max[i] - 10;
                childDecimal = convertToDecimal(childRealDecimal);
                childString = saveBinary(child);
            }
        }
        for (int i = 0; i < dimensions; i++) {
            if (childRealDecimal[i] < min[i] + 10) {
                childRealDecimal[i] = min[i] + 10;
                childDecimal = convertToDecimal(childRealDecimal);
                childString = saveBinary(child);
            }
        }
        child.setBinary(childString);
        child.setDecimal(childDecimal);
        child.setOriginalDecimal(childRealDecimal);
        child.setF_value(f.solvef(child.getOriginalDecimal()));
        child.setFitness(calcFitness(child));
        //System.out.println("Str " + childString[0] + " decimal " + childDecimal[0] + "\tp1   " + parent1.binary[0] + "   p2  " + parent2.binary[0] );
        return child;
    }

    Cromosome mutate(Cromosome c) {
        String[] binary = c.getBinary();
        Cromosome mutated = new Cromosome();
        double[] mutatedDecimal = new double[dimensions];
        for (int i = 0; i < dimensions; i++) {
            int random = ThreadLocalRandom.current().nextInt(1, binary[i].length());
            if (binary[i].charAt(random) == '0') {
                binary[i] = replace(binary[i], random, '1');
            } else binary[i] = replace(binary[i], random, '0');
            mutatedDecimal[i] = Integer.parseInt(binary[i], 2);
        }

        mutated.setBinary(binary);
        mutated.setDecimal(convertToDecimal(mutatedDecimal));
        mutated.setOriginalDecimal(convertToRealDecimal(mutated.getDecimal()));

        mutated.setF_value(f.solvef(mutated.getOriginalDecimal()));
        mutated.setFitness(calcFitness(mutated));
        //System.out.println("Str " + childString[0] + " decimal " + childDecimal[0] + "\tp1   " + parent1.binary[0] + "   p2  " + parent2.binary[0] );
        return mutated;
    }

    public String replace(String str, int index, char replace) {
        if (str == null) {
            return str;
        } else if (index < 0 || index >= str.length()) {
            return str;
        }
        char[] chars = str.toCharArray();
        chars[index] = replace;
        return String.valueOf(chars);
    }


    public void updateFitness() {
        for (int i = 0; i < popSize; i++) {
            population[i].setFitness(calcFitness(population[i]));
        }
    }

}


