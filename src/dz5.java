public class dz5 {

    public static void main(String[] args) {

        int ZAD = 4;

        switch (ZAD) {
            case 1:
                System.out.println("_____TASK 1________");
                Matrix A1 = new Matrix("data/dz5/mat1a.txt");
                Matrix B1 = new Matrix("data/dz5/mat1b.txt");
                Matrix x1 = new Matrix("data/dz5/mat1x.txt");
                Matrix r1 = new Matrix("data/dz5/mat1r.txt");

                double T = 0.01;
                double Tmax = 10;
                int printInterv = 10;
                System.out.println("##############IZRAVNI EULER##############");
                Euler euler = new Euler();
                euler.iterateEuler(x1, A1, B1, r1, T, Tmax, printInterv);
                System.out.println("##############OBRNUTI EULER##############");
                InverseEuler invEu = new InverseEuler();
                invEu.iterateInverseEuler(x1, A1, B1, r1, T, Tmax, printInterv);

                System.out.println("##############TRAPEZNI##############");
                Trapese trap = new Trapese();
                trap.iterateTrapese(x1, A1, B1, r1, T, Tmax, printInterv);

                System.out.println("##############RUNGE KUTTA##############");
                RungeKutta rk = new RungeKutta();
                rk.iterateRungeKutta(x1, A1, B1, r1, T, Tmax, printInterv);

                System.out.println("############## PECE ##############");
                PECE pece = new PECE();
                pece.iteratePECE(x1, A1, B1, r1, T, Tmax, printInterv);

                System.out.println("############## PE(CE)^2 ##############");
                PECECE pecece = new PECECE();
                pecece.iteratePECECE(x1, A1, B1, r1, T, Tmax, printInterv);
                break;

            case 2:
                System.out.println("_____TASK 2________");
                Matrix A2 = new Matrix("data/dz5/mat2a.txt");
                Matrix B2 = new Matrix("data/dz5/mat2b.txt");
                Matrix x2 = new Matrix("data/dz5/mat2x.txt");
                Matrix r2 = new Matrix("data/dz5/mat2r.txt");

                double T2 = 0.1;
                double Tmax2 = 1;
                int printInterv2 = 1;
                System.out.println("##############IZRAVNI EULER##############");
                Euler euler2 = new Euler();
                euler2.iterateEuler(x2, A2, B2, r2, T2, Tmax2, printInterv2);
                System.out.println("##############OBRNUTI EULER##############");
                InverseEuler invEu2 = new InverseEuler();
                invEu2.iterateInverseEuler(x2, A2, B2, r2, T2, Tmax2, printInterv2);

                System.out.println("##############TRAPEZNI##############");
                Trapese trap2 = new Trapese();
                trap2.iterateTrapese(x2, A2, B2, r2, T2, Tmax2, printInterv2);

                System.out.println("##############RUNGE KUTTA##############");
                RungeKutta rk2 = new RungeKutta();
                rk2.iterateRungeKutta(x2, A2, B2, r2, T2, Tmax2, printInterv2);

                System.out.println("############## PECE ##############");
                PECE pece2 = new PECE();
                pece2.iteratePECE(x2, A2, B2, r2, T2, Tmax2, printInterv2);

                System.out.println("############## PE(CE)^2 ##############");
                PECECE pecece2 = new PECECE();
                pecece2.iteratePECECE(x2, A2, B2, r2, T2, Tmax2, printInterv2);
                break;

            case 3:
                System.out.println("_____TASK 3________");
                Matrix A3 = new Matrix("data/dz5/mat3a.txt");
                Matrix B3 = new Matrix("data/dz5/mat3b.txt");
                Matrix x3 = new Matrix("data/dz5/mat3x.txt");
                Matrix r3 = new Matrix("data/dz5/mat3r.txt");

                double T3 = 0.01;
                double Tmax3 = 10;
                int printInterv3 = 10;
                System.out.println("##############IZRAVNI EULER##############");
                Euler euler3 = new Euler();
                euler3.iterateEuler(x3, A3, B3, r3, T3, Tmax3, printInterv3);
                System.out.println("##############OBRNUTI EULER##############");
                InverseEuler invEu3 = new InverseEuler();
                invEu3.iterateInverseEuler(x3, A3, B3, r3, T3, Tmax3, printInterv3);

                System.out.println("##############TRAPEZNI##############");
                Trapese trap3 = new Trapese();
                trap3.iterateTrapese(x3, A3, B3, r3, T3, Tmax3, printInterv3);

                System.out.println("##############RUNGE KUTTA##############");
                RungeKutta rk3 = new RungeKutta();
                rk3.iterateRungeKutta(x3, A3, B3, r3, T3, Tmax3, printInterv3);

                System.out.println("############## PECE ##############");
                PECE pece3 = new PECE();
                pece3.iteratePECE(x3, A3, B3, r3, T3, Tmax3, printInterv3);

                System.out.println("############## PE(CE)^2 ##############");
                PECECE pecece3 = new PECECE();
                pecece3.iteratePECECE(x3, A3, B3, r3, T3, Tmax3, printInterv3);
                break;

            case 4:
                System.out.println("_____TASK 4________");
                Matrix A4 = new Matrix("data/dz5/mat4a.txt");
                Matrix B4 = new Matrix("data/dz5/mat4b.txt");
                Matrix x4 = new Matrix("data/dz5/mat4x.txt");
                Matrix r4 = new Matrix("data/dz5/mat4r.txt");

                double T4 = 0.01;
                double Tmax4 = 1;
                int printInterv4 = 10;
                System.out.println("##############IZRAVNI EULER##############");
                Euler euler4 = new Euler();
                euler4.iterateEuler2(x4, A4, B4, r4, T4, Tmax4, printInterv4);
                System.out.println("##############OBRNUTI EULER##############");
                InverseEuler invEu4 = new InverseEuler();
                invEu4.iterateInverseEuler2(x4, A4, B4, r4, T4, Tmax4, printInterv4);

                System.out.println("##############TRAPEZNI##############");
                Trapese trap4 = new Trapese();
                trap4.iterateTrapese2(x4, A4, B4, r4, T4, Tmax4, printInterv4);

                System.out.println("##############RUNGE KUTTA##############");
                RungeKutta rk4 = new RungeKutta();
                rk4.iterateRungeKutta2(x4, A4, B4, r4, T4, Tmax4, printInterv4);

                System.out.println("############## PECE ##############");
                PECE pece4 = new PECE();
                pece4.iteratePECE2(x4, A4, B4, r4, T4, Tmax4, printInterv4);

                System.out.println("############## PE(CE)^2 ##############");
                PECECE pecece4 = new PECECE();
                pecece4.iteratePECECE2(x4, A4, B4, r4, T4, Tmax4, printInterv4);
                break;

        }
    }
}
