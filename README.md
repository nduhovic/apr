# APR
Contains:<br />
Matrix class for matrix operations(Includes LU and LUP decomposition, matrix inverse and calculating determinant)<br />
Optimisation algorithms:<br />
-Golden Section search<br />
-Coordinate search<br />
-Hooke Jeeves<br />
-Gradient descent<br />
-Nelder Med simplex<br />
-Newton Raphson<br />
-Constraints transform<br />
Numerical integration algorithms:<br />
-Euler<br />
-Inverse Euler<br />
-Trapese<br />
-Runge Kutta<br />
-PECE(Predictor: Euler + Corrector: Trapese)<br /> 
-PE(CE)^2 (Predictor: Euler + Corrector: Inverse Euler)<br />
//TODO Genetic algorithm (supports float and binary)<br />


