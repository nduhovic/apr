public class dz1 {

    public static void main(String[] args) {

        //int ZAD = 10;
        for (int ZAD = 1; ZAD < 11; ZAD++) {
            try {
                switch (ZAD) {
                    case 1:
                        System.out.println("_______________\nZADATAK 1:");
                        Matrix A1 = new Matrix("data/zad1A.txt");
                        Matrix B1 = new Matrix("data/zad1B.txt");
                        if (A1.isEqual(B1)) System.out.println("Matrices are equal");
                        else System.out.println("Matrices are not equal");
                        B1.printMatrix();
                        B1.scalarMultiply(5.000000001);
                        B1.scalarMultiply(1 / 5.000000001);
                        B1.printMatrix();
                        break;
                    case 2:
                        System.out.println("_______________\nZADATAK 2:");
                        String LUorLUP = "LUP";
                        Matrix A2 = new Matrix("data/zad2A.txt");
                        Matrix B2 = new Matrix("data/zad2B.txt");
                        Matrix A2copy = new Matrix(A2);
                        Matrix B2copy = new Matrix(B2);
                        if (LUorLUP == "LU") {
                            System.out.println("LU Decomposition:");
                            A2.LUDecomposition();
                        } else if (LUorLUP == "LUP") {
                            System.out.println("LUP Decomposition:\n");
                            Matrix P2 = new Matrix(3);
                            A2copy.LUPDecomposition(P2);
                            System.out.println("LU=");
                            A2copy.printMatrix();
                            System.out.println("_________" + "\nSolution:");
                            P2.matrixMultiply(B2copy);
                            P2.forwardSubstitution(A2copy);
                            P2.backwardSubstitution(A2copy);
                            P2.printMatrix();
                        }
                        break;
                    case 3:
                        System.out.println("_______________\nZADATAK 3:");
                        String LUorLUP3 = "LUP";
                        Matrix A3 = new Matrix("data/zad3A.txt");
                        Matrix B3 = new Matrix("data/zad3B.txt");
                        Matrix A3copy = new Matrix(A3);
                        Matrix B3copy = new Matrix(B3);
                        if (LUorLUP3 == "LU") {
                            System.out.println("LU Decomposition:");
                            A3.LUDecomposition();
                            A3.printMatrix();
                        } else if (LUorLUP3 == "LUP") {
                            System.out.println("LUP Decomposition:\n");
                            Matrix P3 = new Matrix(3);
                            A3copy.LUPDecomposition(P3);
                            System.out.println("LU=");
                            A3copy.printMatrix();
                            System.out.println("_________" + "\nSolution:");
                            P3.matrixMultiply(B3copy);
                            P3.forwardSubstitution(A3copy);
                            P3.backwardSubstitution(A3copy);
                            P3.printMatrix();

                        }
                        break;
                    case 4:
                        System.out.println("_______________\nZADATAK 4:");
                        Matrix A4 = new Matrix("data/zad4A.txt");
                        Matrix B4 = new Matrix("data/zad4B.txt");
                        Matrix A4copy = new Matrix(A4);
                        Matrix B4copy = new Matrix(B4);

                        System.out.println("LU Decomposition:");
                        A4.eps = 10e-7;
                        A4.LUDecomposition();
                        System.out.println("LU=");
                        A4.printMatrix();
                        System.out.println("_________" + "\nSolution:");
                        B4.forwardSubstitution(A4);
                        B4.backwardSubstitution(A4);
                        B4.printMatrix();

                        System.out.println("LUP Decomposition:\n");
                        Matrix P4 = new Matrix(3);
                        A4copy.LUPDecomposition(P4);
                        System.out.println("LU=");
                        A4copy.printMatrix();
                        System.out.println("_________" + "\nSolution:");
                        P4.matrixMultiply(B4copy);
                        P4.forwardSubstitution(A4copy);
                        P4.backwardSubstitution(A4copy);
                        P4.printMatrix();

                        break;

                    case 5:
                        System.out.println("_______________\nZADATAK 5:");
                        Matrix A5 = new Matrix("data/zad5A.txt");
                        Matrix B5 = new Matrix("data/zad5B.txt");
                        Matrix A5copy = new Matrix(A5);
                        Matrix B5copy = new Matrix(B5);


                        System.out.println("LUP Decomposition:\n");
                        Matrix P5 = new Matrix(3);
                        A5copy.LUPDecomposition(P5);
                        System.out.println("LU=");
                        A5copy.printMatrix();
                        System.out.println("_________" + "\nSolution:");
                        P5.matrixMultiply(B5copy);
                        P5.forwardSubstitution(A5copy);
                        P5.backwardSubstitution(A5copy);
                        P5.printMatrix();

                        break;

                    case 6:
                        System.out.println("_______________\nZADATAK 6:");
                        Matrix A6 = new Matrix("data/zad6A.txt");
                        Matrix B6 = new Matrix("data/zad6B.txt");
                        Matrix A6copy = new Matrix(A6);
                        Matrix B6copy = new Matrix(B6);

                        System.out.println("LU Decomposition:");
                        A6.eps = 10e-15;
                        A6.LUDecomposition();
                        System.out.println("LU=");
                        A6.printMatrix();
                        System.out.println("_________" + "\nSolution:");
                        B6.forwardSubstitution(A6);
                        B6.backwardSubstitution(A6);
                        B6.printMatrix();

                        System.out.println("\nLUP Decomposition:\n");
                        Matrix P6 = new Matrix(3);
                        A6copy.eps = 10e-15;
                        A6copy.LUPDecomposition(P6);
                        System.out.println("LU=");
                        A6copy.printMatrix();
                        System.out.println("_________" + "\nSolution:");
                        P6.matrixMultiply(B6copy);
                        P6.forwardSubstitution(A6copy);
                        P6.backwardSubstitution(A6copy);
                        P6.printMatrix();

                        break;

                    case 7:
                        System.out.println("_______________\nZADATAK 7:");
                        Matrix A7 = new Matrix("data/zad7A.txt");
                        System.out.println("LUP Decomposition:\n");
                        Matrix P7 = new Matrix(3);
                        A7.inverse(P7);
                        System.out.println("Inverse=");
                        A7.printMatrix();

                        break;

                    case 8:
                        System.out.println("_______________\nZADATAK 8:");
                        Matrix A8 = new Matrix("data/zad8A.txt");
                        System.out.println("LUP Decomposition:\n");
                        Matrix P8 = new Matrix(3);
                        A8.inverse(P8);
                        System.out.println("Inverse=");
                        A8.printMatrix();

                        break;

                    case 9:
                        System.out.println("_______________\nZADATAK 9:");
                        Matrix A9 = new Matrix("data/zad8A.txt");
                        System.out.println("Determinant:\n");
                        System.out.println("Determinant=" + A9.determinant());
                        break;

                    case 10:
                        System.out.println("_______________\nZADATAK 10:");
                        Matrix A10 = new Matrix("data/zad2A.txt");
                        System.out.println("Determinant:\n");
                        System.out.println("Determinant=" + A10.determinant());
                        break;

                    case 0:
                        Matrix A = new Matrix("data/zad1A.txt");
                        Matrix B = new Matrix("data/zad1B.txt");
                        A.printMatrix();
                        Matrix P = new Matrix(4);
                        A.LUPDecomposition(P);
                        System.out.println("___________");
                        A.printMatrix();
                        System.out.println("___________");
                        P.matrixMultiply(B);
                        P.printMatrix();
                        P.forwardSubstitution(A);
                        P.printMatrix();
                        P.backwardSubstitution(A);
                        A.printMatrix();
                        System.out.println("______solution_____");
                        P.printMatrix();

                        break;

                    case -1:
                        Matrix Aa = new Matrix("data/zad1A.txt");
                        Aa.printMatrix();
                        Matrix Pp = new Matrix(Aa.getCols());
                        Aa.LUPDecomposition(Pp);
                        for (int i = 1; i < Aa.getCols(); i++) {

                        }


                        break;
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

}
