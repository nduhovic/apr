package genetic;

public interface FunctionGA {
    double solvef(double x[]);
}
