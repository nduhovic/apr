import java.io.*;
import java.util.Scanner;

/**
 * Matrix class
 * Contains constructors for creating matrices and methods for manipulating them.
 * Also contains methods for LU decomposition, LUP decomposition, backward and forward
 * substitution.
 * <p>
 * Assumes that you know what you're doing so some dummy checks are not written
 */
public class Matrix {
    public int rows;             // number of rows
    public int cols;             // number of columns
    public double[][] data;      // 2d array, matrix contents
    public double eps = 10e-6;
    public int swaps;            //stores how many row swaps were executed


    /**
     * Constructor for creating permutation (square) matrix
     *
     * @param rows matrix size
     */
    public Matrix(int rows) {
        this.rows = rows;
        this.cols = rows;
        data = new double[rows][rows];
        for (int i = 0; i < rows; i++)
            this.data[i][i] = 1;
        this.swaps = 0;
    }

    /**
     * Copy constructor
     *
     * @param A matrix that is being copied
     */
    public Matrix(Matrix A) {
        this.data = new double[A.getRows()][A.getCols()];
        this.rows = A.getRows();
        this.cols = A.getCols();
        for (int i = 0; i < this.rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                this.data[i][j] = A.getElement(i, j);
            }
        }
    }

    /**
     * Constructor for creating matrix based on 2d array
     *
     * @param data data with matrix content
     */
    public Matrix(double[][] data) {
        rows = data.length;
        cols = data[0].length;
        this.data = new double[rows][cols];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                this.data[i][j] = data[i][j];
    }

    /**
     * Constructor for creating matrix by reading data from file
     *
     * @param filename name of the file with data for creating matrix(path)
     */
    public Matrix(String filename) {
        File file = new File(filename);
        try {
            Scanner input = new Scanner(file);
            // pre-read in the number of rows/columns; ugly because checks columns row times
            int rows = 0;
            int columns = 0;
            while (input.hasNextLine()) {
                ++rows;
                Scanner colReader = new Scanner(input.nextLine());
                columns = 0;
                while (colReader.hasNextDouble()) {
                    colReader.nextDouble();
                    ++columns;
                }
            }
            double[][] a = new double[rows][columns];
            input.close();

            // read in the data
            input = new Scanner(file);
            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < columns; ++j) {
                    if (input.hasNextDouble()) {
                        a[i][j] = input.nextDouble();
                    }
                }
            }
            this.data = a;
            this.rows = data.length;
            this.cols = data[0].length;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public double[][] getData() {
        return data;
    }

    /**
     * Method for adding one matrix to another
     *
     * @param B matrix that is being added
     */
    public void addMatrix(Matrix B) {
        if (this.rows == B.rows && this.cols == B.cols) {
            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < cols; ++j) {
                    this.data[i][j] += B.data[i][j];
                }
            }
        } else System.out.println("Cannot add matrix of different size!");
    }

    public Matrix getCol(int i) {
        Matrix copy = new Matrix(this);
        //copy.printMatrix();
        double[][] data = new double[copy.getRows()][1];
        int rows = copy.getRows();

        for (int j = 0; j < rows; ++j) {
            data[j][0] = copy.getElement(j, i);
        }
        Matrix matrix = new Matrix(data);
        //matrix.printMatrix();
        return matrix;

    }

    /**
     * Method for subtracting matrices
     *
     * @param B matrix is subtracted by this matrix
     */
    public void subtractMatrix(Matrix B) {
        if (this.rows == B.rows && this.cols == B.cols) {
            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < cols; ++j) {
                    this.data[i][j] -= B.data[i][j];
                }
            }
        } else System.out.println("Cannot subtract matrix of different size!");
    }

    /**
     * Method that multiplies matrix with scalar
     *
     * @param scalar
     */
    public void scalarMultiply(Double scalar) {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                this.data[i][j] *= scalar;
            }
        }
    }

    /**
     * Method for multiplying two matrices
     *
     * @param B
     */
    public void matrixMultiply(Matrix B) {
        Matrix A = this;
        if (this.cols != B.rows) throw new RuntimeException("Illegal matrix dimensions.");
        double temp[][] = new double[this.rows][B.cols];
        for (int i = 0; i < this.rows; i++)
            for (int j = 0; j < B.cols; j++)
                for (int k = 0; k < this.cols; k++)
                    temp[i][j] += (A.data[i][k] * B.data[k][j]);
        this.data = temp;
        this.rows = data.length;
        this.cols = data[0].length;
    }

    /**
     * Method fot transposing matrix
     */
    public void transpose() {
        double[][] temp = new double[cols][rows];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                temp[j][i] = this.data[i][j];
            }
        }
        this.data = temp;
        this.rows = data.length;
        this.cols = data[0].length;
    }

    /**
     * Method that prints matrix to standard output
     */
    public void printMatrix() {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                System.out.print(data[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Method for retrieving specific element from matrix
     *
     * @param row
     * @param col
     * @return element on [row][col]
     */
    public double getElement(int row, int col) {
        return data[row][col];
    }

    /**
     * Checks of two matrixes are equals. Ignores small(<10e-6) differences
     *
     * @param B matrix that it's being compared to
     * @return true if equal, false otherwise
     */
    public boolean isEqual(Matrix B) {
        if (this.rows != B.rows || this.cols != B.cols) return false;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (Math.abs(this.data[i][j] - B.data[i][j]) > eps) return false;
            }
        }
        return true;
    }

    /**
     * Method that writes matrix to file.
     *
     * @param filename file to which matrix is to be written to
     * @param matrix   matrix that is being written to file
     */
    void writeMatrixToFile(String filename, Matrix matrix) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(filename));

            for (int i = 0; i < matrix.data.length; i++) {
                for (int j = 0; j < matrix.data[i].length; j++) {
                    bw.write(matrix.data[i][j] + " ");
                }
                bw.newLine();
            }
            bw.flush();
        } catch (IOException e) {
        }
    }

    /**
     * Method that implements forward substitution using same memory space for B.
     * L*Y=B
     * After completion B=Y
     *
     * @param L
     */
    public void forwardSubstitution(Matrix L) {
        for (int i = 0; i < this.rows - 1; i++) {
            for (int j = i + 1; j < this.rows; j++) {
                this.data[j][0] -= L.data[j][i] * this.data[i][0];
            }
        }
    }

    /**
     * Method that implements forward substitution using same memory space for Y.
     * U*X=Y
     * After completion X=Y
     *
     * @param U
     */
    public void backwardSubstitution(Matrix U) {
        for (int i = this.rows - 1; i >= 0; i--) {
            this.data[i][0] /= U.data[i][i];
            for (int j = 0; j < i; j++) {
                this.data[j][0] -= U.data[j][i] * this.data[i][0];
            }
        }
    }

    /**
     * Method that implements LU decomposition. Decomposes A to L*U using same
     * memory space
     */
    public void LUDecomposition() {
        for (int i = 0; i < this.rows; i++) {
            for (int j = i + 1; j < this.rows; j++) {
                this.data[j][i] /= this.data[i][i];
                for (int k = i + 1; k < this.rows; k++) {
                    this.data[j][k] -= this.data[j][i] * this.data[i][k];
                }
            }
            if (Math.abs(this.data[i][i]) < eps)
                throw new RuntimeException("Zero or close to zero on the diagonal. Not good :(");
        }
    }

    /**
     * Method that switches two matrix rows
     *
     * @param i
     * @param j
     */
    public void switchRows(int i, int j) {
        Matrix copy = new Matrix(this);
        double temp[][] = copy.data;
        for (int k = 0; k < this.rows; k++) {
            this.data[i][k] = this.data[j][k];
            this.data[j][k] = copy.data[i][k];
        }
    }

    /**
     * Method that implements LUP decomposition. Decomposes A to L*U using same
     * memory space
     *
     * @param P permutation matrix needed for storing which rows were switched
     */
    public void LUPDecomposition(Matrix P) {
        for (int i = 0; i < this.rows; i++) {
            int pivot = i;
            for (int j = i + 1; j < this.rows; j++) {
                if (Math.abs(this.getElement(j, i)) > Math.abs(this.getElement(pivot, i)))
                    pivot = j;
            }
            this.switchRows(i, pivot);
            P.switchRows(i, pivot);
            if (pivot != i) this.swaps++;

            for (int j = i + 1; j < this.rows; j++) {
                this.data[j][i] /= this.data[i][i];
                for (int k = i + 1; k < this.rows; k++) {
                    this.data[j][k] -= this.data[j][i] * this.data[i][k];
                }
            }
            // check if singular
            if (Math.abs(this.data[i][i]) < eps)
                throw new RuntimeException("Zero or close to zero on the diagonal. Not good :(");
        }
    }

    /**
     * Method that implements LUP decomposition. Decomposes A to L*U using same
     * memory space
     *
     * @param P permutation matrix needed for storing which rows were switched
     */
    public void inverse(Matrix P) {
        //System.out.println("Matrica : \n");
        //this.printMatrix();
        this.LUPDecomposition(P);
        //System.out.println("LUP DEKOMPOZICIJA : \n");
        //this.printMatrix();
        Matrix inverseMatrix = new Matrix(this.data);

        for (int i = 0; i < this.getRows(); i++) {
            Matrix copy = P.getCol(i);
            copy.forwardSubstitution(this);
            copy.backwardSubstitution(this);
            //System.out.println("Rjesenje");
            //copy.printMatrix();
            for (int j = 0; j < this.getCols(); j++)
                inverseMatrix.data[j][i] = copy.getElement(j, 0);
        }
        this.data = inverseMatrix.data;
        //inverseMatrix.printMatrix();
    }

    public double determinant() {
        Matrix copy = new Matrix(this.data);
        Matrix P = new Matrix(this.getRows());
        copy.LUPDecomposition(P);
        double product = 1;
        for (int i = 0; i < copy.getRows(); i++) product *= copy.getElement(i, i);
        double det = Math.pow(-1, copy.swaps) * product;
        return det;

    }
}


