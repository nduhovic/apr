public class RungeKutta {
    public Matrix solveRungeKutta(Matrix x1, Matrix A1, Matrix B1, Matrix r, double T) {

        Matrix A = new Matrix(A1);
        Matrix B = new Matrix(B1);
        Matrix x = new Matrix(x1);
        B.matrixMultiply(r);
        //m1 = A*x + B*r
        Matrix m1 = new Matrix(A);
        m1.matrixMultiply(x);
        m1.addMatrix(B);

        //m2
        Matrix m2 = new Matrix(A);
        Matrix m1copy2 = new Matrix(m1);
        Matrix xcopy2 = new Matrix(x);
        m1copy2.scalarMultiply(T / 2.0);
        xcopy2.addMatrix(m1copy2);
        m2.matrixMultiply(xcopy2);
        m2.addMatrix(B);

        //m3
        Matrix m3 = new Matrix(A);
        Matrix m2copy3 = new Matrix(m2);
        Matrix xcopy3 = new Matrix(x);
        m2copy3.scalarMultiply(T / 2.0);
        xcopy3.addMatrix(m2copy3);
        m3.matrixMultiply(xcopy3);
        m3.addMatrix(B);

        //m4
        Matrix m4 = new Matrix(A);
        Matrix m3copy4 = new Matrix(m3);
        Matrix xcopy4 = new Matrix(x);
        m3copy4.scalarMultiply(T);
        xcopy4.addMatrix(m3copy4);
        m4.matrixMultiply(xcopy4);
        m4.addMatrix(B);

        //xk+1 = xk + T/6 ( m1 + 2m2 + 2m3 + m4)

        m2.scalarMultiply(2.0);
        m3.scalarMultiply(2.0);
        m1.addMatrix(m2);
        m1.addMatrix(m3);
        m1.addMatrix(m4);
        m1.scalarMultiply(T / 6.0);
        x.addMatrix(m1);
        //x.printMatrix();
        return x;
    }

    public void iterateRungeKutta(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {
        Matrix rkSolved0 = solveRungeKutta(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double analiticX1 = x.data[0][0] * Math.cos(i + T) + x.data[1][0] * Math.sin(i + T);
            double analiticX2 = x.data[1][0] * Math.cos(i + T) - x.data[0][0] * Math.sin(i + T);
            diff += Math.abs(rkSolved0.data[0][0] - analiticX1) + Math.abs(rkSolved0.data[1][0] - analiticX2);

            Matrix rkSolvednext = solveRungeKutta(rkSolved0, A, B, r, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + rkSolved0.getElement(0, 0) + "\t\t" + rkSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            rkSolved0 = rkSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }

    public void iterateRungeKutta2(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {
        Matrix rkSolved0 = solveRungeKutta(x, A, B, r, T);
        double i = 0;
        int iter = 0;
        do {
            double[][] new_r = {{i}, {i}};
            Matrix new_r_mat = new Matrix(new_r);
            Matrix rkSolvednext = solveRungeKutta(rkSolved0, A, B, new_r_mat, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + rkSolved0.getElement(0, 0) + "\t\t" + rkSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            rkSolved0 = rkSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
    }
}
