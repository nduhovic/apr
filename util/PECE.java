public class PECE {
    public Matrix solvePECE(Matrix x, Matrix A, Matrix B, Matrix r, double T) {
        Euler euler = new Euler();
        Trapese trapese = new Trapese();
        Matrix x_i = new Matrix(x);

        x_i = euler.solveEuler(x, A, B, r, T);

        //trapezni, korekcija
        Matrix Xcopy = new Matrix(x);
        Matrix Acopy = new Matrix(A);
        Matrix Acopy2 = new Matrix(A);
        Matrix Bcopy = new Matrix(B);
        Matrix Bcopy2 = new Matrix(B);
        Acopy.matrixMultiply(x);
        Bcopy.matrixMultiply(r);
        Acopy2.matrixMultiply(x_i);
        Bcopy2.matrixMultiply(r);

        Acopy.addMatrix(Bcopy);
        Acopy.addMatrix(Acopy2);
        Acopy.addMatrix(Bcopy2);
        Acopy.scalarMultiply(T / 2);
        Xcopy.addMatrix(Acopy);

        return Xcopy;
    }

    public void iteratePECE(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {

        Matrix PECESolved0 = solvePECE(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double analiticX1 = x.data[0][0] * Math.cos(i + T) + x.data[1][0] * Math.sin(i + T);
            double analiticX2 = x.data[1][0] * Math.cos(i + T) - x.data[0][0] * Math.sin(i + T);
            diff += Math.abs(PECESolved0.data[0][0] - analiticX1) + Math.abs(PECESolved0.data[1][0] - analiticX2);


            Matrix trapSolvednext = solvePECE(PECESolved0, A, B, r, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + PECESolved0.getElement(0, 0) + "\t\t" + PECESolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            PECESolved0 = trapSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }

    public void iteratePECE2(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {

        Matrix PECESolved0 = solvePECE(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double[][] new_r = {{i}, {i}};
            Matrix new_r_mat = new Matrix(new_r);

            Matrix trapSolvednext = solvePECE(PECESolved0, A, B, new_r_mat, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + PECESolved0.getElement(0, 0) + "\t\t" + PECESolved0.getElement(1, 0));

            }
            PECESolved0 = trapSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);

    }
}
