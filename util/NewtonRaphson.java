import functions.FunctionHelper;
import functions.FunctionInterface;

import java.util.Arrays;

public class NewtonRaphson {
    public void solveNewtonRaphson(FunctionInterface f, double[] x0, boolean goldenRatio, double eps) {

        // delta_x= -inverz(H) * vektor_gradijenta

        double[] deltaX = {1, 1};
        double[] x = Arrays.copyOf(x0, x0.length);
        int iter = 0;
        int divCounter = 0;
        double oldFx = f.solvef(x);
        double newFx;

        do {
            iter++;
            Matrix H = new Matrix(f.solve_hessian(x));
            Matrix P = new Matrix(H.rows);
            H.inverse(P);
            H.scalarMultiply(-1.0);
            double[][] temp = {f.solvef_1d(x)};
            Matrix grad = new Matrix(temp);
            grad.transpose();
            H.matrixMultiply(grad);
            //H.printMatrix();

            for (int i = 0; i < x0.length; i++) {
                deltaX[i] = H.data[i][0];

            }
            if (goldenRatio) {
                for (int i = 0; i < deltaX.length; i++) {
                    double halp[] = Arrays.copyOf(x, x.length);
                    FunctionHelper fun = new FunctionHelper(i, halp, f, deltaX);
                    double x0u[] = new double[x0.length];
                    double[] unim = GoldenSectionSearch.unimodalInterval(fun, 1, x0u);
                    double l_min = GoldenSectionSearch.goldenSection(fun, unim, eps);
                    x[i] += (l_min * deltaX[i]);
                }
            } else {
                for (int i = 0; i < x.length; i++) {
                    x[i] += deltaX[i];
                }
            }
            newFx = f.solvef(x);
            if (newFx < oldFx) {
                divCounter = 0;
            } else {
                divCounter++;
            }
            if (divCounter >= 100) {
                System.out.println("Divergencija!");
                break;
            }
            //System.out.println(iter + "\tdeltax " + Arrays.toString(deltaX));
        } while (!checkCondition(deltaX, eps));
        System.out.println("Minimum: " + Arrays.toString(x));
        System.out.println("F(x_min): " + newFx);
    }

    private boolean checkCondition(double[] deltaX, double eps) {
        boolean flag = true;
        for (int i = 0; i < deltaX.length; i++) {
            if (Math.abs(deltaX[i]) > eps) {
                flag = false;
            }
        }
        return flag;
    }
}
