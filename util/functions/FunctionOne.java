package functions;

import java.util.Arrays;

public class FunctionOne implements FunctionInterface {
    int i;
    double[] x0;
    FunctionInterface f;
    public int counter = 0;
    public int gradCounter = 0;
    public int hessCounter = 0;

    public FunctionOne(double[] x0) {
        this.i = i;
        this.x0 = x0;
        this.f = f;

    }

    @Override
    public double solvef(double[] x) {
        this.incCounter();
        return 100 * ((x[1] - (x[0] * x[0])) * (x[1] - (x[0] * x[0]))) + ((1 - x[0]) * (1 - x[0]));

    }

    @Override
    public double[] solvef_1d(double[] x) {
        incGradCounter();
        double[] copy = Arrays.copyOf(x0, x0.length);
        double der_x1 = 400 * x[0] * x[0] * x[0] + (2 - 400 * x[1]) * x[0] - 2;
        double der_x2 = 200 * (x[1] - x[0] * x[0]);
        return new double[]{der_x1, der_x2};
    }

    void incCounter() {
        this.counter++;
    }

    public void resetCounter() {
        this.counter = 0;
    }

    void incGradCounter() {
        this.gradCounter++;
    }

    public void resetGradCounter() {
        this.gradCounter = 0;
    }

    void incHessCounter() {
        this.hessCounter++;
    }

    public void resetHessCounter() {
        this.hessCounter = 0;
    }

    public void resetAllCounters() {
        this.counter = 0;
        this.gradCounter = 0;
        this.hessCounter = 0;
    }

    public double[][] solve_hessian(double[] x) {
        incHessCounter();
        double der_x1_x1 = -400 * (x[1] - x[0] * x[0]) - 800 * x[1] * x[1] + 2;
        double der_x1_x2 = -400 * x[0];
        double der_x2_x1 = -400 * x[0];
        double der_x2_x2 = 200;
        return new double[][]{{der_x1_x1, der_x1_x2,}, {der_x2_x1, der_x2_x2}};
    }
}
