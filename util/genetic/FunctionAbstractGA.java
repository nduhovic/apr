package genetic;

public abstract class FunctionAbstractGA implements FunctionGA {

    public int counter = 0;

    public FunctionAbstractGA() {
        super();
    }

    public int getCounter() {
        return counter;
    }

    public void incCounter() {
        this.counter++;
    }

    public void resetCounter() {
        this.counter = 0;
    }
}


