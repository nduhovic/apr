import functions.FunctionAbstract;

import java.util.concurrent.ThreadLocalRandom;

public class dz2 {
    public static void main(String[] args) {
        double eps = 10e-6;
        int ZAD = 5;

        //functions
        FunctionAbstract f = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return Math.pow(x[0] - 3, 2) + Math.pow(x[1] - 3, 2);
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };
        FunctionAbstract f1 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return 100 * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]) + (1 - x[0]) * (1 - x[0]);
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };
        FunctionAbstract f2 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return (x[0] - 4) * (x[0] - 4) + 4 * (x[1] - 2) * (x[1] - 2);
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }

        };
        FunctionAbstract f3 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                double sum = 0;
                for (int i = 0; i < x.length; i++) {
                    sum = sum + ((x[i] - (i + 1)) * (x[i] - (i + 1)));
                }
                return sum;
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };
        FunctionAbstract f4 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return Math.abs((x[0] - x[1]) * (x[0] + x[1])) + Math.sqrt((x[0] * x[0]) + (x[1] * x[1]));
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };
        FunctionAbstract f6 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                double sum = 0;
                for (int i = 0; i < x.length; i++) {
                    sum = sum + (x[i] * x[i]);
                }
                double a = (Math.sin(Math.sqrt(sum)) * Math.sin(Math.sqrt(sum))) - 0.5;
                double b = 1 + (0.001 * sum);
                return 0.5 + (a / (b * b));
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };


        switch (ZAD) {
            case 1:
                System.out.println("_____TASK_1______\n");
                System.out.println("\t\tx0 = 10");
                System.out.println("---function-0---\t---Coordinate-search---");
                double x0[] = {10};
                CoordinateSearch cs0 = new CoordinateSearch();
                cs0.solveCoordinate(f, x0, eps);
                System.out.println("No. calculations: " + f.getCounter());

                f.resetCounter();
                System.out.println("---function-0---\t---Nelder-Mead-simplex---");
                NelderMead nm0 = new NelderMead();
                nm0.solveNelderMead(f, x0);
                System.out.println("No. calculations: " + f.getCounter());

                f.resetCounter();
                System.out.println("---function-0---\t---Hooke-Jeeves---");
                HookeJeeves hj0 = new HookeJeeves();
                hj0.solveHookeJeeves(f, x0, false);
                System.out.println("No. calculations: " + f.getCounter());

                f.resetCounter();
                System.out.println("---function-0---\t---Golden-section-search---");
                double sol = GoldenSectionSearch.goldenSection(f, GoldenSectionSearch.unimodalInterval(f, 1, x0), eps);
                double[] wrap = new double[1];
                wrap[0] = sol;
                System.out.println("x_min = " + sol + "\tf_min = " + f.solvef(wrap));
                System.out.println("No. calculations: " + f.getCounter());

                f.resetCounter();
                System.out.println("\t\tx0 = 100");
                System.out.println("---function-0---\t---Coordinate-search---");
                double x022[] = {100};
                CoordinateSearch cs02 = new CoordinateSearch();
                cs02.solveCoordinate(f, x022, eps);
                System.out.println("No. calculations: " + f.getCounter());

                f.resetCounter();
                System.out.println("---function-0---\t---Nelder-Mead-simplex---");
                NelderMead nm02 = new NelderMead();
                nm02.solveNelderMead(f, x022);
                System.out.println("No. calculations: " + f.getCounter());

                f.resetCounter();
                System.out.println("---function-0---\t---Hooke-Jeeves---");
                HookeJeeves hj02 = new HookeJeeves();
                hj02.solveHookeJeeves(f, x022, false);
                System.out.println("No. calculations: " + f.getCounter());
                f.resetCounter();

                System.out.println("---function-0---\t---Golden-section-search---");
                double sol2 = GoldenSectionSearch.goldenSection(f, GoldenSectionSearch.unimodalInterval(f, 1, x022), eps);
                double[] wrap2 = new double[1];
                wrap2[0] = sol2;
                System.out.println("x_min = " + sol2 + "\tf_min = " + f.solvef(wrap2));
                System.out.println("No. calculations: " + f.getCounter());

                break;

            case 2:
                System.out.println("_____TASK_2______\n");
                System.out.println("---function-1---\t---coordinate-search---");
                double x02[] = {-1.9, 2};
                CoordinateSearch cs = new CoordinateSearch();
                cs.solveCoordinate(f1, x02, eps);
                System.out.println("No. calculations: : " + f1.getCounter());

                f1.resetCounter();
                System.out.println("---function-1---\t---nelder-mead-simplex---");
                NelderMead nm2 = new NelderMead();
                nm2.solveNelderMead(f1, x02);
                System.out.println("No. calculations: : " + f1.getCounter());

                f1.resetCounter();
                System.out.println("---function-1---\t---hooke-jeeves---");
                HookeJeeves hj = new HookeJeeves();
                hj.solveHookeJeeves(f1, x02, false);
                System.out.println("No. calculations: : " + f1.getCounter());
                f1.resetCounter();

                System.out.println("______________________\n");
                System.out.println("---function-2---\t---coordinate-search---");
                double x0f2[] = {0.1, 0.3};
                CoordinateSearch cs2f2 = new CoordinateSearch();
                cs2f2.solveCoordinate(f2, x0f2, eps);
                System.out.println("No. calculations: : " + f2.getCounter());

                f2.resetCounter();
                System.out.println("---function-2---\t---nelder-mead-simplex---");
                NelderMead nm2f2 = new NelderMead();
                nm2f2.solveNelderMead(f2, x0f2);
                System.out.println("No. calculations: : " + f2.getCounter());

                f2.resetCounter();
                System.out.println("---function-2---\t---hooke-jeeves---");
                HookeJeeves hj2f2 = new HookeJeeves();
                hj2f2.solveHookeJeeves(f2, x0f2, false);
                System.out.println("No. calculations: : " + f2.getCounter());
                f2.resetCounter();

                System.out.println("______________________\n");
                System.out.println("---function-3---\t---coordinate-search---");
                double x0f3[] = {1, 2, 3, 4, 5};
                CoordinateSearch cs2f3 = new CoordinateSearch();
                cs2f3.solveCoordinate(f3, x0f3, eps);
                System.out.println("No. calculations: : " + f3.getCounter());

                f3.resetCounter();
                System.out.println("---function-3---\t---nelder-mead-simplex---");
                NelderMead nm2f3 = new NelderMead();
                nm2f3.solveNelderMead(f3, x0f3);
                System.out.println("No. calculations: : " + f3.getCounter());

                f3.resetCounter();
                System.out.println("---function-3---\t---hooke-jeeves---");
                HookeJeeves hj2f3 = new HookeJeeves();
                hj2f3.solveHookeJeeves(f3, x0f3, false);
                System.out.println("No. calculations: : " + f3.getCounter());
                f3.resetCounter();

                System.out.println("______________________\n");
                System.out.println("---function-4---\t---coordinate-search---");
                double x0f4[] = {5.1, 1.1};
                CoordinateSearch cs2f4 = new CoordinateSearch();
                cs2f4.solveCoordinate(f4, x0f4, eps);
                System.out.println("No. calculations: : " + f4.getCounter());

                f4.resetCounter();
                System.out.println("---function-4---\t---nelder-mead-simplex---");
                NelderMead nm2f4 = new NelderMead();
                nm2f4.solveNelderMead(f4, x0f4);
                System.out.println("No. calculations: : " + f4.getCounter());

                f4.resetCounter();
                System.out.println("---function-4---\t---hooke-jeeves---");
                HookeJeeves hj2f4 = new HookeJeeves();
                hj2f4.solveHookeJeeves(f4, x0f4, false);
                System.out.println("No. calculations: : " + f4.getCounter());
                f4.resetCounter();

                break;

            case 3:
                System.out.println("_____TASK_3______\n");
                f4.resetCounter();
                System.out.println("---function-4---\t---nelder-mead-simplex---");
                double x0c3f4[] = {5, 5};
                NelderMead nmc3f4 = new NelderMead();
                nmc3f4.solveNelderMead(f4, x0c3f4);
                System.out.println("No. calculations: : " + f4.getCounter());

                f4.resetCounter();
                System.out.println("---function-4---\t---hooke-jeeves---");
                HookeJeeves hjc3f4 = new HookeJeeves();
                hjc3f4.solveHookeJeeves(f4, x0c3f4, false);
                System.out.println("No. calculations: : " + f4.getCounter());
                f4.resetCounter();
                break;

            case 4:
                System.out.println("_____TASK_4______\n");
                double x0c4f1[] = {0.5, 0.5};
                System.out.println("---function-1---\t---nelder-mead-simplex---");
                System.out.println("step=\t0.5");
                NelderMead nmc4 = new NelderMead();
                nmc4.setOffset(0.5);
                nmc4.solveNelderMead(f1, x0c4f1);
                System.out.println("No. calculations: : " + f1.getCounter());
                for (int i = 1; i <= 20; i++) {
                    System.out.println("step=\t" + i);
                    f1.resetCounter();
                    nmc4.setOffset(i);
                    //System.out.println("---function-1---\t---nelder-mead-simplex---");
                    nmc4.solveNelderMead(f1, x0c4f1);
                    System.out.println("No. calculations: : " + f1.getCounter());
                }
                f1.resetCounter();
                break;

            case 5:
                System.out.println("_____TASK_5______\n");
                int counter = 0;
                for (int i = 0; i < 10000; i++) {
                    double random1 = ThreadLocalRandom.current().nextDouble(-50, 50);
                    double random2 = ThreadLocalRandom.current().nextDouble(-50, 50);
                    double x0c5[] = {random1, random2};
                    CoordinateSearch csc5 = new CoordinateSearch();
                    if (f6.solvef(csc5.solveCoordinate(f6, x0c5, eps)) < 10E-4) counter++;
                }
                double percentage = counter * 1.0 / 100;
                //System.out.println("counter " + counter + "percentage: " + percentage + "%");
                System.out.println("_____hooke_jeeves______\n");
                int counter2 = 0;
                for (int i = 0; i < 10000; i++) {
                    double random3 = ThreadLocalRandom.current().nextDouble(-50, 50);
                    double random4 = ThreadLocalRandom.current().nextDouble(-50, 50);
                    double x0c52[] = {random3, random4};
                    HookeJeeves hjc5 = new HookeJeeves();
                    if (f6.solvef(hjc5.solveHookeJeeves(f6, x0c52, false)) < 10E-4) counter2++;
                }
                double percentage2 = counter2 * 1.0 / 100;
                System.out.println("coordinate\t counter " + counter + "/10000\tpercentage: " + percentage + "%");
                System.out.println("hooke jeeves\t counter " + counter2 + "/10000\tpercentage: " + percentage2 + "%");
        }
    }
}
