import functions.FunctionInterface;

import java.util.Arrays;

public class ConstraintsTransform {

    /**
     * Method that implements Function Transform into Problem Without Constraints
     * <p>
     * z
     *
     * @param f  function
     * @param g  inequality equations(constraints)
     * @param h  quality equations(constraints)
     * @param x0 initial point
     * @param t  parameter of transformation (t=1/r)
     */
    public void solveConstraintsTransform(FunctionInterface f, FunctionInterface[] g, FunctionInterface[] h, double[] x0, double t, double eps) {
        if (!checkImplicitIneq(x0, g)) {
            System.out.println("Pocetna tocka ne zadovoljava ogranicenja");
            x0 = fixInitialPoint(x0, g);
            System.out.println("Nova pocetna tocka: " + Arrays.toString(x0));
        }
        double[] x_min = Arrays.copyOf(x0, x0.length);
        double[] x_min2 = Arrays.copyOf(x0, x0.length);
        for (int i = 0; i < 10; i++) {

            FunctionInterface transformedF = new FunctionTransformed(f, g, h, t);
            FunctionInterface transformedF2 = new FunctionTransformed(f, g, h, t);
            //System.out.println("u 1. tocki f: " + transformedF.solvef(x_min2));
            HookeJeeves hj = new HookeJeeves();
            x_min = new double[x0.length];
            x_min = hj.solveHookeJeeves(transformedF, x_min, false);
            NelderMead nm = new NelderMead();
            x_min2 = nm.solveNelderMead(transformedF2, x_min2);
            //System.out.println("Minimum HJ:" + Arrays.toString(x_min));
            //System.out.println("Minimum simpleks" + Arrays.toString(x_min2));
            x0 = Arrays.copyOf(x_min2, x_min2.length);
            t *= 10;
        }
        System.out.println("Minimum simpleks" + Arrays.toString(x_min2));
        System.out.println("F(x_min) simpleks: " + f.solvef(x_min2));
        //System.out.println("Minimum HJ:" + Arrays.toString(x_min));
        //System.out.println("F(x_min)HJ: " + f.solvef(x_min));
    }

    boolean checkPoint(double[] x0, FunctionInterface[] g, FunctionInterface[] h) {
        if (checkImplicitIneq(x0, g)) return false;
        if (checkImplicitEq(x0, h)) return false;
        return true;
    }

    boolean checkImplicitIneq(double[] x, FunctionInterface[] constrains) {
        boolean flag = true;
        for (int i = 0; i < constrains.length; i++) {
            if (constrains[i].solvef(x) < 0) {
                flag = false;
                //System.out.println("Implicitno ograničenje nije zadovoljeno");
                break;
            }
            //System.out.println("Implicit OK");
        }
        return flag;
    }

    boolean checkImplicitEq(double[] x, FunctionInterface[] constrains) {
        boolean flag = true;
        for (int i = 0; i < constrains.length; i++) {
            if (constrains[i].solvef(x) != 0) {
                flag = false;
                //System.out.println("Implicitno ograničenje nije zadovoljeno");
                break;
            }
            //System.out.println("Implicit OK");
        }
        return flag;
    }

    double[] fixInitialPoint(double[] x0, FunctionInterface[] g) {
        FunctionInterface f = new FunctionFixPoint(x0, g);
        HookeJeeves hj = new HookeJeeves();
        double[] fixed = hj.solveHookeJeeves(f, x0, false);
        return fixed;
    }

    public class FunctionFixPoint implements FunctionInterface {

        double[] x0;
        FunctionInterface[] g;

        public FunctionFixPoint(double[] x0, FunctionInterface[] g) {

            this.x0 = x0;
            this.g = g;

        }

        @Override
        public double solvef(double[] x) {
            double sum = 0.0;
            double tiValue = 0;
            for (int i = 0; i < this.g.length; i++) {
                double giValue = g[i].solvef(x);
                if (giValue < 0) {
                    tiValue = 1;
                } else {
                    tiValue = 0;
                }
                sum = sum + (tiValue * giValue);
            }
            return -sum;
        }

        @Override
        public double[] solvef_1d(double[] x) {
            return null;
        }

        @Override
        public double[][] solve_hessian(double[] x) {
            return null;
        }
    }

    ;

    public class FunctionTransformed implements FunctionInterface {


        FunctionInterface f;
        FunctionInterface[] g;
        FunctionInterface[] h;
        double t;

        public FunctionTransformed(FunctionInterface f, FunctionInterface[] g, FunctionInterface[] h, double t) {


            this.f = f;
            this.g = g;
            this.h = h;
            this.t = t;

        }

        @Override
        public double solvef(double[] x0) {
            double sum = 0.0;
            double ineqTotal = 0.0;
            double eqTotal = 0.0;
            //System.out.println("t: " + t);
            if (!checkImplicitIneq(x0, g)) {
                ineqTotal = 1E10;
            } else {
                for (int i = 0; i < this.g.length; i++) {
                    ineqTotal += Math.log(this.g[i].solvef(x0));
                }
            }
            for (int i = 0; i < this.h.length; i++) {
                eqTotal += Math.pow(this.h[i].solvef(x0), 2);
            }
            sum += this.f.solvef(x0) - ((1 / this.t) * ineqTotal) + (this.t * eqTotal);
            //System.out.println(sum);
            return sum;
        }

        @Override
        public double[] solvef_1d(double[] x) {
            return null;
        }

        @Override
        public double[][] solve_hessian(double[] x) {
            return null;
        }
    }

    ;
}
