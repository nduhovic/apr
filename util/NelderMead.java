import functions.FunctionInterface;

import java.util.Arrays;

public class NelderMead {

    private double alfa = 1;
    private double beta = 0.5;
    private double gamma = 2;
    private double sigma = 0.5;
    private double offset = 0.5;
    private double eps = 10e-6;

    public void setOffset(double offset) {
        this.offset = offset;
    }


    public double[] solveNelderMead(FunctionInterface f, double[] x0) {
        int dimension = x0.length;
        double[] xc = new double[dimension];
        double[] xk = new double[dimension];
        double[] xe = new double[dimension];
        double[] xr = new double[dimension];
        int h;
        int l;


        //izracunaj tocke simpleksa X[i], i = 0..n;
        double[][] simplexPoints = new double[x0.length + 1][x0.length];
        simplexPoints[0] = Arrays.copyOf(x0, x0.length);
        for (int i = 1; i < x0.length + 1; i++) {
            simplexPoints[i] = Arrays.copyOf(x0, x0.length);
            simplexPoints[i][i - 1] += offset;
        }

        boolean flag = true;
        int counter = 0;
        do {
            counter++;
            //odredi indekse h,l : F(X[h]) = max, F(X[l]) = min;
            // odredi centroid Xc;


            double value = f.solvef(simplexPoints[0]);
            double max = value;
            double min = value;
            h = 0;
            l = 0;
            for (int i = 0; i <= dimension; i++) {
                value = f.solvef(simplexPoints[i]);
                // System.out.println("I: " + i + " value :" + value);
                if (value >= max) {
                    max = value;
                    h = i;
                }
                if (value < min) {
                    min = value;
                    l = i;
                }
            }
            //System.out.println("Min/max:" + l + " / " + h);

            double[] xh = Arrays.copyOf(simplexPoints[h], simplexPoints.length);
            double[] xl = Arrays.copyOf(simplexPoints[l], simplexPoints.length);

            xc = centroid(simplexPoints, h, dimension).clone();                    //odredi centroid


            //System.out.println("CENTROID" + Arrays.toString(xc)+"\n********");

            xr = reflection(simplexPoints, xc, h).clone();      // odredi refleksiju
            //System.out.println("refleksija" + Arrays.toString(xr));
            if (f.solvef(xr) < f.solvef(simplexPoints[l])) {    // ako F(Xr)<F(X[l])
                //TODO
                xe = expansion(xr, xc).clone();                         //odredi ekspanziju
                //System.out.println("ekspanzija" + Arrays.toString(xe));
                double xeValue = f.solvef(xe);
                if (f.solvef(xe) < f.solvef(simplexPoints[l])) {    // ako F(Xe)<F(X[l])
                    simplexPoints[h] = xe.clone();                       // X[h] = Xe;
                } else simplexPoints[h] = xr;                    // inace X[h] = Xr;/
            } else {
                if (check(f, simplexPoints, xr)) {              //  ako F(Xr)>F(X[j]) za svaki j=0..n, j!=h
                    //TODO
                    if (f.solvef(xr) < f.solvef(simplexPoints[h])) simplexPoints[h] = xr;
                    xk = contraction(xc, simplexPoints, h).clone();
                    //System.out.println("kontrakcija" + Arrays.toString(xk));
                    if ((f.solvef(xk)) < f.solvef(simplexPoints[h])) {
                        simplexPoints[h] = xk;
                        //System.out.println("xk"+Arrays.toString(xk));
                        //System.out.println("simplexpoints"+Arrays.toString(simplexPoints[h]));
                    } else {
                        //System.out.println("simplex before" + Arrays.deepToString(simplexPoints));
                        simplexPoints = moveSimplexPoints(simplexPoints, l, xl);
                        //System.out.println("simplex after" + Arrays.deepToString(simplexPoints));

                    }
                } else simplexPoints[h] = xr;
            }
            if (counter >= 10000) break;
            flag = checkCondition(f, simplexPoints, xc);
        } while (flag);
        //System.out.println("iterations" + counter);
        //System.out.println("x_min = " + Arrays.toString(xc) + "\tf_min = " + f.solvef(xc));
        //System.out.println("f_min = " + f.solvef(xc));
        return xc;
    }

    private boolean check(FunctionInterface f, double[][] simplexPoints, double[] xr) {
        boolean flag = true;
        for (int i = 0; i < simplexPoints.length; i++) {
            if (f.solvef(xr) < f.solvef(simplexPoints[i])) flag = false;
        }
        return flag;
    }

    private boolean checkCondition(FunctionInterface f, double[][] simplexPoints, double[] xc) {

        boolean flag = false;

        for (int i = 0; i < xc.length; i++) {
            double suma = 0;
            for (int j = 0; j < simplexPoints.length; j++) {
                suma += Math.pow((f.solvef(simplexPoints[j]) - f.solvef(xc)), 2);
            }
            if (Math.sqrt(suma / xc.length) > eps) {
                flag = true;
            }
        }
        return flag;
    }

    private double[] centroid(double[][] x, int h, int dimension) {
        double[] c = new double[dimension];
        for (int i = 0; i < dimension; i++) {
            c[i] = 0.0;
        }


        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension + 1; j++) {
                c[i] += x[j][i];
                //System.out.println("zbroj"+c[j]);

            }
        }
        for (int i = 0; i < dimension; i++) {  //remove worst point from sum
            c[i] -= x[h][i];
        }
        for (int i = 0; i < dimension; i++) {
            c[i] /= dimension;
        }

        //System.out.println("centorid" + Arrays.toString(c));
        return c;
    }

    private double[] reflection(double[][] x, double[] xc, int h) {
        double[] xr = new double[xc.length];
        for (int i = 0; i < xc.length; i++) {
            xr[i] = (1 + alfa) * xc[i] - alfa * x[h][i];
        }
        return xr;
    }

    private double[] expansion(double[] xc, double[] xr) {
        double[] xe = new double[xc.length];
        for (int i = 0; i < xe.length; i++) {
            xe[i] = (1 - gamma) * xc[i] + gamma * xr[i];
        }
        return xe;
    }

    private double[] contraction(double[] xc, double[][] x, int h) {
        double[] xk = new double[xc.length];
        for (int i = 0; i < xc.length; i++) {
            xk[i] = (1 - beta) * xc[i] + beta * x[h][i];
        }
        return xk;
    }

    private double[][] moveSimplexPoints(double[][] x, int l, double[] xl) {
        //TODO
        double[][] newSimplex = new double[3][2];


        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x.length - 1; j++) {
                if (i == l) newSimplex[i][j] = xl[j];
                else newSimplex[i][j] = sigma * (x[i][j] + xl[j]);
            }
        }
        //System.out.println("move simplex" + Arrays.deepToString(newSimplex));
        return newSimplex;
    }

}