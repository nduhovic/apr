package functions;

public abstract class FunctionAbstract implements FunctionInterface {

    public int counter = 0;

    public FunctionAbstract() {
        super();
    }

    public int getCounter() {
        return counter;
    }

    public void incCounter() {
        this.counter++;
    }

    public void resetCounter() {
        this.counter = 0;
    }
}


