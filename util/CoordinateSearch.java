import functions.Function2;
import functions.FunctionAbstract;

import java.util.Arrays;

public class CoordinateSearch {

    /**
     * Coordinate search
     *
     * @param f  function
     * @param x0 initial search coordinates
     * @return x solution
     **/

    public double[] solveCoordinate(FunctionAbstract f, double[] x0, double eps) {

        boolean flag;
        int dimension = x0.length;
        double x[] = Arrays.copyOf(x0, dimension);


        do {
            flag = false;
            double xs[] = Arrays.copyOf(x, dimension);

            for (int i = 0; i < x.length; i++) {
                double temp[] = Arrays.copyOf(x, dimension);
                Function2 fun = new Function2(i, temp, f);
                //System.out.println(pomocni);;
                double x0u[] = new double[x0.length];
                double[] unim = GoldenSectionSearch.unimodalInterval(fun, 1, x0u);
                double l_min = GoldenSectionSearch.goldenSection(fun, unim, eps);
                x[i] += l_min;
                //System.out.println(x);
            }

            double[] check = new double[dimension];

            for (int i = 0; i < x.length; i++) {
                check[i] = x[i] - xs[i];
            }

            for (int i = 0; i < x.length; i++) {
                if (check[i] > eps) {
                    flag = true;
                }
            }

        } while (flag == true);

        //System.out.println("x_min = " + Arrays.toString(x));
        System.out.println("x_min = " + Arrays.toString(x) + "\tf_min = " + f.solvef(x));


        return x;
    }


}