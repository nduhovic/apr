import functions.*;

public class dz3 {

    public static void main(String[] args) {
        double eps = 1E-6;
        double x1[] = {-1.9, 2};
        double x2[] = {0.1, 0.3};
        double x3[] = {0, 0};
        double x4[] = {0, 0};

        FunctionAbstract condition1 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return x[1] - x[0];
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };

        FunctionAbstract condition2 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return 2 - x[0];
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };

        FunctionAbstract conditionIneq1 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return 3 - x[0] - x[1];
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };

        FunctionAbstract conditionIneq2 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return 3 + (1.5 * x[0]) - x[1];
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };

        FunctionAbstract conditionEqq1 = new FunctionAbstract() {
            @Override
            public double solvef(double[] x) {
                this.incCounter();
                return x[1] - 1;
            }

            @Override
            public double[] solvef_1d(double[] x) {
                return null;
            }

            @Override
            public double[][] solve_hessian(double[] x) {
                return null;
            }
        };

        FunctionOne f1 = new FunctionOne(x1);
        FunctionTwo f2 = new FunctionTwo(x2);
        FunctionThree f3 = new FunctionThree(x3);
        FunctionFour f4 = new FunctionFour(x4);

        int ZAD = 5;
        switch (ZAD) {
            case 1:
                System.out.println("____Task 1___F3________");
                System.out.println("_______________");
                GradientDescent gd = new GradientDescent();
                gd.solveGradientDescent(f3, x3, true, eps);
                System.out.println("Broj evaluacija funkcije: " + f3.counter);
                System.out.println("Broj izračuna gradijenta: " + f3.gradCounter);
                System.out.println("Broj izračuna H: " + f3.hessCounter);

                f3.resetAllCounters();
                System.out.println("_______________");
                GradientDescent gd2 = new GradientDescent();
                gd2.solveGradientDescent(f3, x3, false, eps);
                System.out.println("Broj evaluacija funkcije: " + f3.counter);
                System.out.println("Broj izračuna gradijenta: " + f3.gradCounter);
                System.out.println("Broj izračuna H: " + f3.hessCounter);
                break;

            case 2:
                System.out.println("____Task 2___F1__F2______");
                System.out.println("_____Gradient Descent F1__________");
                GradientDescent gd21 = new GradientDescent();
                gd21.solveGradientDescent(f1, x1, true, eps);
                System.out.println("Broj evaluacija funkcije: " + f1.counter);
                System.out.println("Broj izračuna gradijenta: " + f1.gradCounter);
                System.out.println("Broj izračuna H: " + f1.hessCounter);
                f1.resetAllCounters();
                System.out.println("_____Newton Raphson F1__________");
                NewtonRaphson nr21 = new NewtonRaphson();
                nr21.solveNewtonRaphson(f1, x1, true, eps);
                System.out.println("Broj evaluacija funkcije: " + f1.counter);
                System.out.println("Broj izračuna gradijenta: " + f1.gradCounter);
                System.out.println("Broj izračuna H: " + f1.hessCounter);
                f1.resetAllCounters();
                System.out.println("\n_____Gradient Descent F2__________");
                GradientDescent gd22 = new GradientDescent();
                gd22.solveGradientDescent(f2, x2, true, eps);
                System.out.println("Broj evaluacija funkcije: " + f2.counter);
                System.out.println("Broj izračuna gradijenta: " + f2.gradCounter);
                System.out.println("Broj izračuna H: " + f2.hessCounter);
                f2.resetAllCounters();
                System.out.println("_____Newton Raphson F2__________");
                NewtonRaphson nr22 = new NewtonRaphson();
                nr22.solveNewtonRaphson(f2, x2, true, eps);
                System.out.println("Broj evaluacija funkcije: " + f2.counter);
                System.out.println("Broj izračuna gradijenta: " + f2.gradCounter);
                System.out.println("Broj izračuna H: " + f2.hessCounter);
                f2.resetAllCounters();
                break;
            case 3:
                System.out.println("____Task 3___F1__F2______");
                System.out.println("________Box_F1______");
                Box box = new Box();
                double[] min = {-100, -100};
                double[] max = {100, 100};
                FunctionInterface[] consrt = {condition1, condition2};
                box.solveBox(f1, x1, 1.3, eps, min, max, consrt);
                System.out.println("Broj evaluacija funkcije: " + f1.counter);
                System.out.println("Broj izračuna gradijenta: " + f1.gradCounter);
                System.out.println("Broj izračuna H: " + f1.hessCounter);
                f1.resetAllCounters();

                System.out.println("________Box_F2______");
                Box box2 = new Box();

                box2.solveBox(f2, x2, 1.3, eps, min, max, consrt);
                System.out.println("Broj evaluacija funkcije: " + f2.counter);
                System.out.println("Broj izračuna gradijenta: " + f2.gradCounter);
                System.out.println("Broj izračuna H: " + f2.hessCounter);
                f2.resetAllCounters();
                break;

            case 4:
                System.out.println("____Task 4___F1__F2______");
                System.out.println("____F1_____");
                ConstraintsTransform ct = new ConstraintsTransform();
                FunctionInterface[] consrtIneq = {condition1, condition2};
                FunctionInterface[] consrtEq = {};
                ct.solveConstraintsTransform(f1, consrtIneq, consrtEq, x1, 1, eps);
                System.out.println("Broj evaluacija funkcije: " + f1.counter);
                System.out.println("Broj izračuna gradijenta: " + f1.gradCounter);
                System.out.println("Broj izračuna H: " + f1.hessCounter);
                f1.resetAllCounters();

                System.out.println("____F2_____");
                ConstraintsTransform ct2 = new ConstraintsTransform();
                ct2.solveConstraintsTransform(f2, consrtIneq, consrtEq, x2, 1, eps);
                System.out.println("Broj evaluacija funkcije: " + f2.counter);
                System.out.println("Broj izračuna gradijenta: " + f2.gradCounter);
                System.out.println("Broj izračuna H: " + f2.hessCounter);
                f2.resetAllCounters();
                break;

            case 5:
                System.out.println("____Task 5___F4______");
                System.out.println("____F4_____");
                ConstraintsTransform ct5 = new ConstraintsTransform();
                double x5[] = {5, 5};
                FunctionFour f5 = new FunctionFour(x5);
                FunctionInterface[] consrtIneq2 = {conditionIneq1, conditionIneq2};
                FunctionInterface[] consrtEq2 = {conditionEqq1};
                ct5.solveConstraintsTransform(f5, consrtIneq2, consrtEq2, x5, 1, eps);
                System.out.println("Broj evaluacija funkcije: " + f5.counter);
                System.out.println("Broj izračuna gradijenta: " + f5.gradCounter);
                System.out.println("Broj izračuna H: " + f5.hessCounter);
                f5.resetAllCounters();
                break;


        }


    }
}
