package functions;

import java.util.Arrays;

public class FunctionHelper implements FunctionInterface {

    int i;
    double[] x0;
    FunctionInterface f;
    double[] gradient;

    public FunctionHelper(int i, double[] x0, FunctionInterface f, double[] gradient) {
        this.i = i;
        this.x0 = x0;
        this.f = f;
        this.gradient = gradient;

    }

    @Override
    public double[] solvef_1d(double[] x) {
        return null;
    }


    @Override
    public double solvef(double[] x) {
        double lambda = x[0];// posto je vektor jednodimenzionalni kod unimodalnog i zlatnog reza
        double[] copy = Arrays.copyOf(x0, x0.length);

        copy[i] += lambda * gradient[i];
        return f.solvef(copy);

    }

    @Override
    public double[][] solve_hessian(double[] x) {
        return null;
    }
}