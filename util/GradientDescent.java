import functions.FunctionHelper;
import functions.FunctionInterface;

import java.util.Arrays;

public class GradientDescent {
    public void solveGradientDescent(FunctionInterface f, double[] x0, boolean goldenRatio, double eps) {
        double[] gradient = new double[x0.length];
        double[] x = Arrays.copyOf(x0, x0.length);
        int iter = 0;
        int divCounter = 0;

        do {
            double oldFx = f.solvef(x);
            //find gradient
            gradient = f.solvef_1d(x);
            //System.out.println("Gradijent " + Arrays.toString(gradient));
            for (int i = 0; i < x0.length; i++) {
                gradient[i] = -1 * gradient[i];
            }
            //System.out.println("Gradijent " + Arrays.toString(gradient));
            if (goldenRatio) {
                //find lambda in all directions
                for (int i = 0; i < x0.length; i++) {
                    double temp[] = Arrays.copyOf(x, x.length);
                    FunctionHelper fun = new FunctionHelper(i, temp, f, gradient);
                    double x0u[] = new double[x0.length];
                    double[] unim = GoldenSectionSearch.unimodalInterval(fun, 1, x0u);
                    double l_min = GoldenSectionSearch.goldenSection(fun, unim, eps);
                    //System.out.println("lambda: " + (l_min));
                    x[i] += (l_min * gradient[i]);

                }
            } else {
                for (int i = 0; i < x0.length; i++) {
                    x[i] += gradient[i];
                }
            }
            double newFx = f.solvef(x);
            if (newFx < oldFx) {
                divCounter = 0;
            } else {
                divCounter++;
            }
            //System.out.println("iter" + iter + "\t" +Arrays.toString(x));
            iter++;
            if (iter >= 10000) break;
            if (divCounter >= 100) {
                System.out.println("Divergencija!");
                break;
            }
        } while (condition(gradient, eps));
        System.out.println("Minimum:" + Arrays.toString(x));
        System.out.println("F(x:min):" + f.solvef(x));
    }

    private boolean condition(double[] gradient, double eps) {

        double sum = 0;
        for (int i = 0; i < gradient.length; i++) {
            sum += gradient[i] * gradient[i];
        }
        //System.out.println(Math.sqrt(sum));
        if (Math.sqrt(sum) < eps) return false;
        else return true;
    }
}
