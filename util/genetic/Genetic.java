package genetic;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Genetic {
    static FunctionAbstractGA f;
    static int popSize;
    static int dimensions;
    static double[] min;
    static double[] max;
    static double eps;
    public double[][] population;
    int elitismNo;
    int printFreq;
    double muutationChance;
    int fCounterMax = 10000000 * 100;
    int maxiter;


    public Genetic(FunctionAbstractGA f, int popSize, int maxiter, int dimensions, double[] min, double[] max, double mutationChance, double eps, int printFreq) {
        this.f = f;
        this.popSize = popSize;
        this.dimensions = dimensions;
        this.min = min;
        this.max = max;
        this.eps = eps;
        this.muutationChance = mutationChance;
        this.elitismNo = popSize / 2;
        this.printFreq = printFreq;
        this.maxiter = maxiter;
    }

    public double[] solveGenetic() {
        //generate initial population
        this.population = initPopulation();
        updateFitness(this.population);
        System.out.println("______");
        //System.out.println("fsum old" + fSum());

        double[][] new_population = new double[popSize][dimensions + 1];
        int generationNo = 0;
        while (f.getCounter() < fCounterMax) {
            //System.out.println("ITER :" + generationNo);
            generationNo++;
            int countChromosomes = 0;
            //elitism
            double[][] sorted = new double[popSize][dimensions + 1];
            for (int i = 0; i < sorted.length; i++) {
                sorted[i] = Arrays.copyOf(population[i], population[i].length);
            }
            Arrays.sort(sorted, (a, b) -> Double.compare(a[dimensions], b[dimensions]));
            for (int i = 0; i < elitismNo; ++i) {
                new_population[i] = sorted[i];
                countChromosomes++;
            }

            while (countChromosomes < popSize) {
                //crossover
                int selected1 = rouletteWheelSelection();
                int selected2 = rouletteWheelSelection();
                double[] child = new double[dimensions + 2];
                while (selected1 == selected2) selected2 = rouletteWheelSelection();

                double randomCrossoverNo = ThreadLocalRandom.current().nextInt(1, 3);
                if (randomCrossoverNo == 1) {
                    child = crossover1(population[selected1], population[selected2]);
                } else if (randomCrossoverNo == 2) child = crossover2(population[selected1], population[selected2]);

                //mutate
                double mutateRandom = ThreadLocalRandom.current().nextDouble(0, 1);
                if (mutateRandom < muutationChance) {
                    for (int j = 0; j < dimensions; j++) {
                        child[j] = ThreadLocalRandom.current().nextDouble(min[j], max[j]);
                    }
                    child[dimensions] = f.solvef(child);
                    child[dimensions + 1] = calcFitness(child, population);
                }
                new_population[countChromosomes] = child;
                countChromosomes++;
            }

            updateFitness(new_population);
            this.population = new_population;

            //System.out.println("New population : " + Arrays.deepToString(new_population));
            //System.out.println("fsum new" + fSum());
            double fWorst = f.solvef(sorted[popSize - 1]);
            double fBest = f.solvef(sorted[0]);
            if (generationNo % printFreq == 0) {
                double[] bestX = new double[dimensions];
                for (int i = 0; i < dimensions; i++) {
                    bestX[i] = sorted[0][i];
                }
                System.out.println("Generacija: " + generationNo + "\tCurrent best:" + Arrays.toString(bestX) +
                        "\t f(x_best):" + sorted[0][dimensions] + "\t F evaluations: " + f.getCounter());

            }
            if (generationNo > maxiter || Math.abs(sorted[0][dimensions]) < eps) {
                System.out.println("_______FINISHED________");
                double[] bestX = new double[dimensions];
                for (int i = 0; i < dimensions; i++) {
                    bestX[i] = sorted[0][i];
                }
                System.out.println("Generacija: " + generationNo + "\tCurrent best:" + Arrays.toString(bestX) +
                        "\t f(x_best):" + sorted[0][dimensions] + "\t F evaluations: " + f.getCounter());
                break;
            }
        }
        double[][] sorted = new double[popSize][dimensions + 1];
        for (int i = 0; i < sorted.length; i++) {
            sorted[i] = Arrays.copyOf(population[i], population[i].length);
        }
        Arrays.sort(sorted, (a, b) -> Double.compare(a[dimensions], b[dimensions]));

        //System.out.println("x1:"+ sorted[0][0] + "\tx2:\t" + sorted[0][1] + "\t f(x):\t"+ sorted[0][2]);
        //System.out.println(Arrays.deepToString(population));
        //System.out.println("fsum new" + fSum());
        return sorted[0];
    }

    //chromosome: {x1,..., xn, f(x), fitness}
    //population: array of chromosomes
    public double[][] initPopulation() {
        double[][] population = new double[popSize][dimensions + 2];
        for (int i = 0; i < popSize; i++) {
            for (int j = 0; j < dimensions; j++) {
                population[i][j] = ThreadLocalRandom.current().nextDouble(min[j], max[j]);
            }

            population[i][dimensions] = f.solvef(population[i]);
            population[i][dimensions + 1] = 0;
        }
        //calculate fitness init
        updateFitness(population);
        this.population = population;
        return population;
    }

    //total fitness of the population
    public double fitnessSum() {
        //System.out.println("called");
        double sum = 0.0;
        for (int i = 0; i < popSize; i++) {
            sum += this.population[i][this.dimensions + 1];
        }
        return sum;
    }


    public int rouletteWheelSelection() {
        int selected = popSize;
        double fitnessSum = fitnessSum();
        double random = ThreadLocalRandom.current().nextDouble(0, fitnessSum);
        double sum = 0.0;
        for (int i = 0; i < popSize; i++) {
            sum += population[i][dimensions + 1];
            if (sum > random) {
                selected = i;
                break;
            }
        }

        return selected;
    }

    public double calcFitness(double[] chromosome, double[][] pop) {
        double[][] sorted = new double[popSize][dimensions + 1];
        double fitness = 100;
        for (int i = 0; i < sorted.length; i++) {
            sorted[i] = Arrays.copyOf(pop[i], pop[i].length);
        }
        Arrays.sort(sorted, (a, b) -> Double.compare(a[dimensions], b[dimensions]));
        //System.out.println(Arrays.deepToString(sorted));
        double fWorst = f.solvef(sorted[popSize - 1]);
        double fBest = f.solvef(sorted[0]);
        if (fWorst - fBest < eps) fitness = 100;
        else fitness = 1 + ((100 - 1) * ((f.solvef(chromosome) - fWorst) / (fBest - fWorst)));
        if (fitness < 0) fitness = 1;
        return fitness;
    }

    public boolean contains(int i, int[] selected, int iter) {
        boolean flag = false;
        for (int j = 0; j < iter; j++) {
            if (selected[j] == i) flag = true;
        }
        return flag;
    }

    //Aritmatičko
    double[] crossover1(double[] parent1, double[] parent2) {
        double[] child = new double[dimensions + 2];
        for (int i = 0; i < dimensions; i++) {
            double random = ThreadLocalRandom.current().nextDouble(0, 1);
            child[i] = random * parent1[i] + (1 - random) * parent2[i];
            if (child[i] > max[i]) child[i] = max[i] - 1;
            if (child[i] < min[i]) child[i] = min[i] + 1;
        }
        child[dimensions] = f.solvef(child);
        child[dimensions + 1] = calcFitness(child, population);
        return child;
    }

    //Heurističko
    double[] crossover2(double[] parent1, double[] parent2) {
        double[] child = new double[dimensions + 2];
        double[] temp = new double[dimensions + 2];
        if (parent1[dimensions + 1] > parent2[dimensions + 1]) {
            temp = parent2;
            parent2 = parent1;
            parent1 = temp;
        }
        for (int i = 0; i < dimensions; i++) {
            double random = ThreadLocalRandom.current().nextDouble(0, 1);
            child[i] = random * (parent2[i] - parent1[i]) + parent2[i];
            if (child[i] > max[i]) child[i] = max[i] - 1;
            if (child[i] < min[i]) child[i] = min[i] + 1;
        }

        child[dimensions] = f.solvef(child);
        child[dimensions + 1] = calcFitness(child, population);
        return child;
    }

    public void updateFitness(double[][] pop) {
        for (int i = 0; i < popSize; i++) {
            pop[i][dimensions + 1] = calcFitness(pop[i], pop);
        }
    }

    public void updateFValues(double[][] pop) {
        for (int i = 0; i < popSize; i++) {
            pop[i][dimensions + 1] = f.solvef(pop[i]);
        }
    }
}
