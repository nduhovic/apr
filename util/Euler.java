public class Euler {
    public Matrix solveEuler(Matrix x, Matrix A, Matrix B, Matrix r, double T) {

        Matrix U = new Matrix(A.rows);
        Matrix Acopy = new Matrix(A);
        Matrix Bcopy = new Matrix(B);
        //implementation of formula
        //M*xk
        Acopy.scalarMultiply(T);
        U.addMatrix(Acopy);
        U.matrixMultiply(x);
        //N*r(t)
        Bcopy.scalarMultiply(T);
        Bcopy.matrixMultiply(r);

        //add
        U.addMatrix(Bcopy);

        //System.out.println(Arrays.toString(data));
        return U;
    }

    public void iterateEuler(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {
        Euler eu = new Euler();
        Matrix euSolved0 = solveEuler(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double analiticX1 = x.data[0][0] * Math.cos(i + T) + x.data[1][0] * Math.sin(i + T);
            double analiticX2 = x.data[1][0] * Math.cos(i + T) - x.data[0][0] * Math.sin(i + T);
            diff += Math.abs(euSolved0.data[0][0] - analiticX1) + Math.abs(euSolved0.data[1][0] - analiticX2);

            Matrix euSolvednext = solveEuler(euSolved0, A, B, r, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + euSolved0.getElement(0, 0) + "\t\t" + euSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            euSolved0 = euSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }

    public void iterateEuler2(Matrix x, Matrix A, Matrix B, Matrix r, double T, double Tmax, int printInterv) {

        Matrix euSolved0 = solveEuler(x, A, B, r, T);
        double i = 0;
        double diff = 0;
        int iter = 0;
        do {
            double[][] new_r = {{i}, {i}};
            Matrix new_r_mat = new Matrix(new_r);
            Matrix euSolvednext = solveEuler(euSolved0, A, B, new_r_mat, T);
            if (iter % printInterv == 0) {
                System.out.println(iter * T + "\t\t" + euSolved0.getElement(0, 0) + "\t\t" + euSolved0.getElement(1, 0));
                //euSolved0.printMatrix();
            }
            euSolved0 = euSolvednext;
            i = i + T;
            iter++;
        } while (i < Tmax);
        //System.out.println("difference " + diff);
    }

}
